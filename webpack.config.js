const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const { paths, baseConfig } = require("./webpack.base.config");

module.exports = {
    ...baseConfig,
    mode: "development",
    plugins: [
        // enable HMR globally
        new webpack.HotModuleReplacementPlugin(),

        // prints more readable module names in the browser console on HMR updates
        new webpack.NamedModulesPlugin(),

        // for compiling CSS
        new MiniCssExtractPlugin({
            filename: "styles.css",
        }),
    ],
    devServer: {
        inline: true,
        port: 8080,
        hot: true,
        contentBase: paths.public,
        historyApiFallback: {
            rewrites: [
                {
                    from: /\./,
                    to: "/",
                },
            ],
        },
    },
    devtool: "source-map",
};
