const path = require("path");

const paths = {
    public: path.resolve(__dirname, "public"),
    src: path.resolve(__dirname, "src"),
    build: path.resolve(__dirname, "build"),
};

module.exports = {
    paths,
    baseConfig: {
        context: paths.src,
        entry: ["./scripts/main.js"],
        output: {
            path: paths.public,
            publicPath: "/",
            filename: "main.js",
        },

        resolve: {
            extensions: [".ts", ".tsx", ".js", ".jsx"],
        },

        module: {
            rules: [
                {
                    // .ts and .js files
                    test: /\.(t|j)sx?$/,
                    use: ["babel-loader"],
                    exclude: /node_modules/,
                },
                {
                    // sass
                    test: /\.scss$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        "sass-loader", // compiles Sass to CSS
                    ],
                },
                {
                    // css
                    test: /\.css$/,
                    include: /node_modules/,
                    use: ["style-loader", "css-loader"],
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    use: "url-loader",
                },
            ],
        },
    },
};
