//This function loads complex proteins
//import Glmol from 'glmol/js/Three49custom';
//import * as THREE from './glmol/js/Three49custom.js';
//import GLmol from './glmol/js/GLmol.js';

var glmol_source = {}

var glmol_obj = new GLmol('glmol', true);

glmol_obj.defineRepresentation = function(){
	var all = this.getAllAtoms();
	var allHet = this.getHetatms(all);
	var hetatm = this.removeSolvents(allHet);

	this.colorByAtom(all, {});
	this.colorByChain(all);

	var asu = new THREE.Object3D();

	console.log(this.sphereRadius);
	this.drawAtomsAsSphere(asu, hetatm, this.sphereRadius);
	//this.drawBondsAsStick(asu, this.getSidechains(all), this.cylinderRadius, this.cylinderRadius);
   	//this.drawCartoon(asu, all, this.curveWidth, this.thickness);
   	//this.drawSymmetryMates2(this.modelGroup, asu, this.protein.biomtMatrices);
   	this.modelGroup.add(asu);
}


function load_glmol(pdb_url){
	console.log("PDB FILE URL: " + pdb_url);
	// let url = "https://storage.googleapis.com/permm-assets/pdb/permm/" + pdb_filename
	// let url = "https://permm-assets.storage.googleapis.com/pdb/permm/" + pdb_filename
	$.get(pdb_url, function(data){
		
		glmol_source.data = data
		
		$('#glmol_src').val(data);
		glmol_obj.loadMolecule();
		
		glmol_source.models = []
		
		initializeGLMol();
		
	}).done(load_table).done(load_jquery);
}

function load_table(){
	console.log(glmol_source)
	glmol_model_table = $('#glmol_model_table');
	
	table_body = glmol_model_table.find('tbody');
	
	
	
	for (mod in glmol_source.models){
		
		if(mod == glmol_source.models.length - 1 && model != 0){
			continue;
		}
		model = glmol_source.models[mod];
		
		var selected = ""
		
		if(mod == 0){
			selected = 'selected'
		}
		
		table_body.append("<tr id ='glmol_tr_" + (model.model_number - 1) + "' class = 'glmol_tr "+selected+"' onclick='change_model(" +(model.model_number - 1) + ")'> <td>" + model.model_number +"</td> <td>"+model.Z+"</td>  <td>"+ (Math.round(model.energy * 10 ) / 10)+"</td> </tr>" );
		
		
	}
	
	
	
	$('#glmol_src').val(glmol_source.models[0].source.join('\n'));
	
	//console.log($('#glmol_src').val())
	
	glmol_obj.loadMolecule();
}

function initializeGLMol(){
	var lines = glmol_source.data.split('\n');
	var cur_model_number = 0;
	for(var i = 0; i < lines.length; i++){
		if (i == 0){
			//first model
			glmol_source.models.push({});
			glmol_source.models[0].source = [];
			glmol_source.models[0].source.push([' ']);
		}

		glmol_source.models[cur_model_number].source.push([lines[i]]);

		if(lines[i].substring(0, 5) == 'MODEL'){
			var trim = lines[i].trim()
			
			model_number = trim.replace(/^\D+/g, '');
			glmol_source.models[cur_model_number].model_number = model_number;
		}

		else if (lines[i].substring(0, 6) == 'REMARK'){
				if(lines[i].substring(0, 10) == 'REMARK  Z='){
					
					var lineCleaned = lines[i].substring(6, lines[i].length);
					
					var remarkArray = lineCleaned.replace(/ /g,'').split(','); //remove commas, and spit by ' ' space.
					arrayLength = remarkArray.length;
					
					for (var j = 0; j < arrayLength; j++) {
							var propertyArray = (remarkArray[j]).split('=');
							
							console.log("{name:" + propertyArray[0]+ ", value: "+parseFloat(propertyArray[1])+", units: "+propertyArray[1].replace(/[^a-z\\\\]/gi, '')+"}")
							glmol_source.models[cur_model_number][propertyArray[0]] = parseFloat(propertyArray[1]);
					//Do something
					}
					
					
					
				}
		}
		else if (lines[i].substring(0, 6) == 'ENDMDL'){
			cur_model_number += 1
			
			glmol_source.models.push({});
			glmol_source.models[cur_model_number].source = [];
			glmol_source.models[cur_model_number].source.push([" "]);
		}
	}
	
	
	console.log(glmol_source);
}

function change_model(model_number){
	console.log("Model Changed");
	
	$('#glmol_src').val(glmol_source.models[model_number].source.join('\n'));
	
	glmol_obj.rebuildScene();
	glmol_obj.loadMolecule();
}


function load_jquery(){
	$('.glmol_tr').click(function(){
		$(this).siblings().removeClass('selected');
		$(this).addClass('selected')
	})
}



function shift_right(){
	var cur_i = parseInt($('.selected').attr('id').replace(/^\D+/g, ''))
	if(cur_i  + 1< glmol_source.models.length - 1){
		$('.glmol_tr').siblings().removeClass('selected');
		var next_i = cur_i + 1
		$('#glmol_tr_' + next_i).addClass('selected')
		
		change_model(next_i);
	}
}

function shift_left(){
	var cur_i = parseInt($('.selected').attr('id').replace(/^\D+/g, ''))
	if(cur_i > 0){
		$('.glmol_tr').siblings().removeClass('selected');
		var next_i = cur_i - 1
		$('#glmol_tr_' + next_i).addClass('selected')
		
		change_model(next_i);
	}
}

function adjust_iframe_height(){
  var actual_height = document.getElementById("body").scrollHeight;
  parent.postMessage(600, "*")
 }

function load_pdb(pdb_link){
    console.log('load_pdb called');
	 
	window.scrollTo(0,-400);
	 
  adjust_iframe_height();
  load_glmol(pdb_link);
  
  $(".shift-right").click(shift_right);
  $(".shift-left").click(shift_left);
 }

console.log('hello');

load_pdb($('#pdblink').attr("href"));
/*window.on("load", function() {
    console.log('window on load called');

    load_pdb($('#pdbname').text());
});*/
 
 function load_glmol_to_DOM(){
	 console.log($("#pdblink").text());
	 
 }

