# Front End PerMM Project
This Project is the front end of Lomize Group's PerMM database. You can find the back end PerMM code [here](https://bitbucket.org/lomize-group/lomize-permm-backend). This project makes requests to the backend API and then displays the data that is returned. This is a [React.js](https://reactjs.org/), [Redux.js](https://redux.js.org/), and [webpack](https://webpack.js.org/) application. The application is deployed as a static website on Google's [Firebase](https://firebase.google.com/) platform and uses [Google Cloud](https://cloud.google.com/) bucket storage to distribute its assets globally.

Below you will find some information on how to run the code, perform common tasks, and where various data must be located.

## Prerequisites
- Install [Node.js](https://nodejs.org) and [Node Package Manager (NPM)](https://www.npmjs.com/)
- Make sure your computer is running in a Linux based environment. Mac OS X and Linux work great. If you are using Windows you may need to boot inside a Linux partition.

## Running the code
### Get the code
First clone the code repository to your local environment:
```
  mkdir permm-front-end
  cd permm-front-end
  git clone https://<your-username>@bitbucket.org/lomize-group/lomize-permm-frontend.git
  cd lomize-permm-frontend
  git checkout master
```

### Install Dependencies
All dependencies are defined in `package.json`. Run `npm install` in the `lomize-permm-frontend` directory to install all dependencies listed in this file. These dependencies include all development tools and frameworks needed, such as webpack, React.js, and Redux.js.

### Run the code
Some scripts are defined in the `package.json` file for running and building the codebase. One such script is `start`. This script will start webpack development server. Run it with the following command:
```
  npm run start
```
This command will transpile all Javascript code and serve the assets at `localhost:8080`. Webpack will automatically hot-load any changes you make to the project. Additionally, webpack will give output on your terminal if there are any errors building.

## Additional Commands
There are several additional commands that can be found in `package.json :scripts`.\
- `start` - runs the webpack development server.
- `build` - creates a production build of your Javascript code and installed packages. The definition for the production build is contained in `webpack.prod.config.js`.
- `deploy` - creates a production build and deploys the code to Firebase.

If you must install a new package with `npm install`, make sure you include the `--save ` or `--save-dev` flag in the command. `--save` should be used for packages that will be included in the production build; `--save-dev` should be used otherwise. This will add the installed dependency to the `package.json` file.
```
npm install <new-package-name> --save
npm install <new-package-name> --save-dev

# example:
npm install bootstrap --save
npm install babel --save-dev
```

## How this Project is Organized
This project is setup like many classic react.js projects that make use of webpack. 
```
/node_modules
/public
  /images
  index.html
  main.js
  styles.css
/src
  /scripts
    /actions
    /components
    /fonts
    /helpers
    /reducers
    main.js
    ....
    ....
package.json
webpack.config.js
webpack.prod.config.js
....
other files
....
```

### /src/scripts/components
This is where you will find all of the react components residing. These include, Home, Contact, and more. As you are making edits to the website, this is where you will be doing a large majority of work. For example, if I wanted to change the text on the home page, I would go to the home page component contained at /src/scripts/components/home/Home.js and edit the text that I want changed. Additionally I can change the styles of the home component under Home.scss (to be imported in Home.js).

### Main.js
This is the main entry point for the front end application and also serves as the homebase for react-router. This manages all the front-end routes and directs users where they need to go. If you were to add another page, this is where you need to put the front end route for it. 

## Conventions
This is how we name and code things.

### Git
We use the gitflow branching method.

- **/master** is used for the current deployed stable version. If code is on master it is always working and may always be deployed.
- **/feat/about_page** the feat/x convention is used to denote features that developers are working on. These may be broken at any time. The idea is to merge master onto a feat branch, fix changes and then merge back onto master. Then deploy changes that way.
- **Branch_names** are always in underspaced. 
  - ```master``` is the master deployed branch
  - ```feat/x``` is a branch that someone is working on
  - ```pom/x``` is a proof of model that is being used as a dry-run for an experimental technology. This is when we want to use a new technology but we aren't sure what it will break.
- **folder_names** are always in underscored (snake_case).
- **Javscript and SCSS FileNames** always have the first letter of each word capitalized (PascalCase).

### React Components and Style
React components are always in a folder with ComponentName.js a ComponentName.scss. These are always side-by-side in the same folder. The ComponentName.js imports the ComponentName.scss at the top of the file. The .scss and .js get transpiled and minified later by webpack during the build process.

#### Redux
We use redux as the primary datastore for a lot of our front-end data. This makes holding state for the user much easier and faster.


