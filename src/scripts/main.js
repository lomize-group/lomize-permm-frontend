import "@babel/polyfill";
import "bootstrap-sass/assets/stylesheets/_bootstrap.scss";
import "core-js/es6/map";
import "core-js/es6/number";
import "core-js/es6/set";
import "raf/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";
import App from "./App.js";
import About from "./components/about/About.js";
import Acknowledgments from "./components/acknowledgments/Acknowledgments.js";
import Classification from "./components/classification/Classification.js";
import Contact from "./components/contact/Contact.js";
import Download from "./components/download/Download.js";
import Home from "./components/home/Home.js";
import MoleculeViewer from "./components/molecule-viewer/MoleculeViewer.js";
import Molecule from "./components/molecule/Molecule.js";
import PermmServer from "./components/permm_server/PermmServer.js";
import Error404 from "./Error404.js";
import reducer from "./reducers/reducers";


let store = createStore(
	reducer,
	applyMiddleware(logger)
);

const Main = () => {
	return (
		<Provider store={store}>
			<Router>
				<Switch>
					<Route exact path="/" component={(props) => (<App children={<Home />} {...props} />)} />

					<Route exact path="/about" component={(props) => (<App children={<About />} {...props} />)} />
					<Route exact path="/download" component={(props) => (<App children={<Download />} {...props} />)} />
					<Route exact path="/contact" component={(props) => (<App children={<Contact />} {...props} />)} />
					<Route exact path="/acknowledgments" component={(props) => (<App children={<Acknowledgments />} {...props} />)} />

					<Route exact path="/server" component={(props) => (<App children={<PermmServer computationServer="memprot" />} {...props} />)} />
					<Route exact path="/permm_server" component={(props) => (<App children={<PermmServer computationServer="memprot" />} {...props} />)} />
					<Route exact path="/permm_server_cgopm" component={(props) => (<App children={<PermmServer computationServer="cgopm" />} {...props} />)} />
					<Route exact path="/permm_server_local" component={(props) => (<App children={<PermmServer computationServer="local" />} {...props} />)} />

					<Route exact path="/groups" component={(props) => (<App children={<Classification />} {...props} />)} />
					<Route exact path="/groups/:id" component={(props) => (<App children={<Classification />} {...props} />)} />

					<Route exact path="/classes" component={(props) => (<App children={<Classification />} {...props} />)} />
					<Route exact path="/classes/:id" component={(props) => (<App children={<Classification />} {...props} />)} />

					<Route exact path="/membrane_systems" component={(props) => (<App children={<Classification />} {...props} />)} />)} />
					<Route exact path="/membrane_systems/:id" component={(props) => (<App children={<Classification />} {...props} />)} />

					<Route exact path="/molecules" component={(props) => (<App children={<Classification />} {...props} />)} />
					<Route exact path="/molecules/:id" component={(props) => (<App children={<Molecule />} {...props} />)} />
					<Route exact path="/molecules/glmol/:id/:molid" component={(props) => (<App children={<MoleculeViewer />} {...props} />)} />

					<Route exact path="/server/glmol" component={(props) => (<App children={<MoleculeViewer />} {...props} />)} />

					<Route component={Error404} />
				</Switch>
			</Router>
		</Provider>
	);
}

export default Main;

ReactDOM.render(<Main />, document.getElementById("react-container"));
