import React from 'react'
import PropTypes from 'prop-types'
import DOMPurify from 'dompurify';

import Config from '../../Config.js';

class Equation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
          script_loaded: false,
          equation_loaded: false,
          oldMath: props.math
        };

        this.onMathJaxLoaded = () => {
            this.setState({script_loaded: true});
            MathJax.Hub.Config({
                showMathMenu: true,
                tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] },
            });
            window.MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview]);
        }
    }

    componentDidMount() {
        this.preview.innerHTML = this.props.math;

        // this.state.loaded ? MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview]) : loadScript(SCRIPT, this.onLoad)
        const script_id = "mathjax_script";
        const mathjax_src = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_HTMLorMML";
        if (document.getElementById(script_id)) {
            this.onMathJaxLoaded();
        }
        else {
            Config.load_script(mathjax_src, this.onMathJaxLoaded);
        }
        
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if (!nextProps.math) return false
    //     return nextProps.math !== this.state.oldMath
    // }

    // componentDidUpdate(prevProps, prevState) {
    //     this.preview.innerHTML = this.props.math;
    //     MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview]);
    // }

    // componentWillReceiveProps(nextProps) {
    //     this.setState({oldMath: nextProps.math});
    // }

    render() {
        const loading_img = (
            <img
                className={this.state.script_loaded ? "hidden" : ""}
                src={Config.loading_image_small}
                alt="Loading"
            />
        );
        const equation = (
            <span
                id="react-mathjax-preview-result"
                className={!this.state.script_loaded ? "hidden" : ""}
                ref={(node) => {this.preview = node}}
            >
            </span>
        );

        if (this.props.inline) {
            return(
                <span className={this.props.className} id='react-mathjax-preview' style={this.props.style}>
                    {loading_img}{equation}
                </span>
            );
        }

        return (
            <div className={this.props.className} id='react-mathjax-preview' style={this.props.style}>
                {loading_img}{equation}
            </div>
        );
    }
}

Equation.propTypes = {
  className: PropTypes.string,
  style: PropTypes.string,
  math: PropTypes.string,
  inline: PropTypes.bool
};

Equation.defaultProps = {
  math: "",
  className: "",
  inline: false
};

export default Equation;