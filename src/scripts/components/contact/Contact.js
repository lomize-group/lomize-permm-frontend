import React from 'react';

import ImgWrapper from '../../helpers/img_wrapper/ImgWrapper.js';

import Config from '../../Config.js';

import './Contact.scss';

class Contact extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}


	render()
	{
		return(
			<div className="contact">
                <div className="page-header">
                	Contact Us
                </div>
                <p>Contact us for any PerMM related questions.</p>
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-6 centered contact-sect">
							<ImgWrapper 
								src={Config.pics.adrei}
								className="contact-img"
							/>
							<p className="contact-info centered name">
								<a href="https://pharmacy.umich.edu/lomize-group" target="_blank">
									Andrei L. Lomize
								</a>
								, PhD
							</p>
							<p className="contact-info centered role">
								Research Scientist
							</p>

                            <p className="contact-info centered actual-contact-info">
								734-925-2370 (Office)<br/>
                                almz at umich.edu
                            </p>

							<p className="contact-info centered address">
								Room 4056<br/>
								College of Pharmacy<br/>
								University of Michigan<br/>
								Ann Arbor, MI 48109, USA
							</p>
						</div>
						<div className="col-sm-6 centered contact-sect">
							<ImgWrapper 
								src={Config.pics.alexey}
								className="contact-img"
							/>
							<p className="contact-info centered name">
								<a href="https://www.linkedin.com/in/alexey-kovalenko-3455a01b6/" target="_blank">
									Alexey Kovalenko
								</a>
							</p>
							<p className="contact-info centered role">
								B.S. in Computer Science and Biochemistry <br/>
								Software Engineer
							</p>
							<p className="contact-info centered actual-contact-info">
								424-217-9712 <br/>
								alexeyk at umich.edu
							</p>

							<p className="contact-info centered address">
								College of Literature, Science, and the Arts<br/>
								University of Michigan<br/>
								Ann Arbor, MI 48109, USA
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Contact;
