import React from 'react';
import appendQuery from 'append-query';
import './Download.scss';
import Config from '../../Config.js';

class Download extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}

	renderDownloadCSV() {
		let items = [];
		let csv_param = {
			fileFormat: "csv"
		};

		for (let i = 0; i < Config.downloads.length; ++i) {
			const tablename = Config.downloads[i];
			const url = appendQuery(Config.baseUrl + Config.classification[tablename].api.route, csv_param);
			items.push(
				<li>
					<a href={url} download>Download {tablename}</a>
				</li>
			);
		}

		return (
			<div className="download-csv">
				<hr/>
				<h3>CSV Files</h3>
				<ul className="csv-list">
					{items}
				</ul>
				<hr/>
			</div>
		);
	}


	render()
	{
		return(
			<div className="download">
				<div className="page-header">Download Files</div>
				<div className="files">
					<ul>
						<li><a href={Config.bound_states_tar}>Download coordinate files of “Bound States” of all molecules</a></li>
						<li><a href={Config.pathway_tar}>Download coordinate files of “Pathways” of all molecules</a></li>
						<li><a href={Config.source_tar}>Download source coordinate files of all molecules</a></li>
						{/*<li><a href={Config.molecule_list_tar}>Download list of all molecules</a></li>
						<li><a href={Config.full_database_tar}>Download full database (sql)</a></li>*/}
					</ul>
				</div>
				{this.renderDownloadCSV()}
				<br/>
			</div>
		);
	}
}

export default Download;
