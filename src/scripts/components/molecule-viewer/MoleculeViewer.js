// import React from 'react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './MoleculeViewer.scss';
import Config from '../../Config.js';


class MoleculeViewer extends React.Component
{
	constructor(props)
	{
		super(props);
    
    // On a local call
    if(props.currentUrl.params.hasOwnProperty('id') && props.currentUrl.params.hasOwnProperty('molid'))
		{
      this.state = {
        molid: props.currentUrl.params.molid,
        id: props.currentUrl.params.id,
        url: Config.pdb_pathway_file(props.currentUrl.params.molid)
      };
    }
    // On a server call
    else
    {
      this.state = { 
        id: "",
        url: props.currentUrl.query.url 
      }
    }
	}

    componentDidMount () {
        // Import javascript files
        const js_paths = [
          'https://code.jquery.com/jquery-3.1.1.min.js',
          'https://www.gstatic.com/charts/loader.js',
          '/js/glmol/js/Three49custom.js',
          '/js/glmol/js/GLmol.js',
          '/js/permm_glmol.js'
        ];

        for(var i = 0; i < js_paths.length; i++) {
            const script = document.createElement("script");
            script.src = js_paths[i];
            script.async = false;
            document.body.appendChild(script);
        }
    }

	render()
	{
		return (
        <div id="body" className="molecule-viewer" style={{height:'inherit'}}>
          <div id = "wrapper">
            <div className = "content">
              
                <ul>
                    <li className="title"><h3>GLMol Model Viewer</h3>
                        <h4 className={this.state.molid === "" ? "hidden" : ""}>{this.state.molid}</h4>
                        <p className={this.state.id === "" ? "hidden" : ""}><Link to={"/molecules/" + this.state.id}>Return to molecule page</Link></p>
                        <p>Download the <a href={this.state.url} className="external" id="pdblink">Pathway PDB</a></p>
                    </li>
                </ul>

            <div className = "model_view_container">

              <div style={{float: 'left', width: '500px'}} className="glmol_container">
                <div id="glmol" style={{width: '500px', height: '400px', backgroundColor: 'lightgray'}}>

                </div>
                <div className="model-control-arrows">
                    <span className="fa fa-arrow-left fa-2x shift-left" >&lt; Previous</span> <span className="fa fa-arrow-right fa-2x shift-right" aria-hidden="true">Next &gt;</span>
                </div>
                 <div>
                </div>
              </div>
              
             

              <div style = {{width:'300px', float:'right', paddingRight: '50px'}} className= "model_select_table" id = "glmol_model_table">
                <table style = {{width: '100%'}}>
                  <thead>
                    <tr>
                      <th>Model</th>
                      <th>Z (A)</th>
                      <th>Energy (kcal/mol)</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>

            </div>


            </div>
            </div>

            <textarea id = "glmol_src" style={{display: 'none'}} />
          </div>


			)
	}
};

function mapStateToProps(state) {
  return {
    currentUrl: state.currentUrl,
  };
}

export default connect(mapStateToProps)(MoleculeViewer);
