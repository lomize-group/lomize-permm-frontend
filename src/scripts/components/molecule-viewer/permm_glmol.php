<?php
require_once 'header.php';

  echo('<script
  src="glmol/js/jquery-1.7.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 




<script type="text/javascript">
 
 function adjust_iframe_height(){
  var actual_height = document.getElementById("body").scrollHeight;
  parent.postMessage(600, "*")
 }

 $(window).on("load", function(){
	 
	window.scrollTo(0,-400);
	 
  adjust_iframe_height();
  load_glmol($("#pdbname").text());
  
  $(".shift-right").click(shift_right);
  $(".shift-left").click(shift_left);
 });
 
 function load_glmol_to_DOM(){
	 console.log($("#pdbname").text());
	 
 }
</script>

  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style.css" title="style" />
  <link rel="stylesheet" type="text/css" href="permm_model_view_style.css" title="style" />
  
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
');

	$pdboutname = $_GET['pdboutname'];
  $molecule_id = $_GET['molecule_id'];


echo('<div id="body" style="height:inherit;">
  <div id = "wrapper" style = "min-height:0px;">
    


    <div class = "content">
      
          <h1>GLMol Model Viewer</h1>
          <h3 id="pdbname">'.$pdboutname.'</h3>


    <div class = "model_view_container">

      <div style="float: left; width: 500px;" class="glmol_container">
        <div id="glmol" style="width: 500px; height: 400px; background-color: lightgray;">

        </div>
		<div class="model-control-arrows">
			<i class="fa fa-arrow-left fa-2x shift-left" ></i> <i class="fa fa-arrow-right fa-2x shift-right" aria-hidden="true"></i>
		</div>
		 <div>
			<a href="molecule_view.php?molecule_id='.$molecule_id.'">Return to molecule page</a>
		</div>
      </div>
	  
	 

      <div style = "width:300px; float:right; padding-right: 50px;"class = "model_select_table" id = "glmol_model_table">
        <table style = "width:100%;">
          <thead>
            <tr>
              <th>Model</th>
              <th>Z (A)</th>
              <th>Energy (kcal/mol)</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>

    </div>



    </div>
  </div>

  <textarea id = "glmol_src" style="display:none;"></textarea>
  
    <script src="glmol/js/Three49custom.js"></script>
  <script src="glmol/js/GLmol.js"></script>
  <script src="permm_glmol.js"></script>
  </div><!-- end of body -->');
  require_once 'footer.php';
?>
