import React, { Component } from 'react';
import './About.scss';
import ScrollableAnchor from 'react-scrollable-anchor';
import Equation from '../equation/Equation.js';

class About extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}

	render()
	{
		return(
			<div className="about">
                <div className="page-header">
                    PerMM Method
                </div>
				<div className="about-text">
					<p>
						PerMM is a novel thermodynamics-based computational method for fast prediction of
						passive permeability of structurally diverse molecules across different membranes. The
						method operates with atomic 3D structures of molecules and represents anisotropic
						properties of lipid bilayers of different composition by transbilayers profiles of dielectric and
						hydrogen-bonding capacity parameters. These profiles have been derived for several
						artificial and natural membranes from distributions of volume fractions of lipid groups
						determined by X-ray and neutron scattering and from surface hydrophobicity of membrane
						proteins along the bilayer normal
						(<a href="https://www.ncbi.nlm.nih.gov/pubmed/23811361" target="_blank" className="external">PubMed</a>).
					</p>
					<p>
						The transfer energy of a molecule from water to each position within the lipid bilayer is
						calculated by the PPM 2.0 method
						(<a href="https://opm.phar.umich.edu/ppm_server" target="_blank" className="external">PPM 2.0</a>)
						sing the implicit solvent model of the DOPC bilayer
						(<a href="https://www.ncbi.nlm.nih.gov/pubmed/23811361" target="_blank" className="external">PubMed</a>).
						The lowest free energy translocation pathway as described by the corresponding &Delta;G(z) curve.
						The permeability coefficient, P<sub>calc</sub>, is determined by integrating the curve:
					</p>
					<div className="paragraph-equation">
						<Equation inline className="equation" math={"`1/(P_(calc)) = int_(-d//2)^(d//2) dz / (K(z)D(z))`"}/>
						<span className="equation-number">(1)</span>
					</div>
					<br/>
					<p>
						where <i>K(z)</i> is the depth-dependent partition coefficient from water to the bilayer; <i>D</i> is the
						diffusion coefficient; <i>z</i> is the translational coordinate of the molecule across the lipid bilayer.
					</p>

					<p>
						The dependency of the partition coefficient <i>K(z)</i> on the Gibbs free energy
						(<i>&Delta;G<sub>transf</sub>(z)</i>) of a molecule along the membrane normal is calculated as:
					</p>
					<div className="paragraph-equation">
						<Equation inline className="equation" math={"`K(z) = e^(-\Delta G_(trans f) (z) \\ // \\ RT)`"}/>
						<span className="equation-number">(2)</span>
					</div>
					<p>
						The optimal translocation pathway of molecules is defined by calculating the optimal spatial
						position and conformation of a molecule at every point of the transmembrane trajectory.
						PerMM calculates: (1) the equilibrium spatial arrangement and binding energy of a molecule
						to membrane; (2) free energy profile of a molecule along the membrane normal; and (3)
						the permeability coefficient. Please
						see <a href="http://sunshine.phar.umich.edu/cellpm/instruction.htm" target="_blank" className="external">instructions</a> for more details.
					</p>
					<p>
						The database provides the results of calculation by PerMM of permeability coefficients of
						small organic molecules and drug-like compounds together with experimental data on
						permeability of these molecules obtained in different artificial and natural systems, including
						BLM, PAMPA, Caco-2/MDCK cell-based and BBB permeability assays.
					</p>
					<p>
						Calculation of intrinsic permeability coefficients of a compound through artificial (BLM,
						PAMPA-DS) and natural membranes (BBB and Caco-2/MDCK) is based on the following
						equations:
					</p>
					<div className="multi-equation">
						<Equation inline className="equation" math={"`logP_(\Sigma)^(BLM) = -log\( ASA \int_(-d//2)^(d//2) dz / (K(z)) \)`"}/>
						<span className="equation-number">(3)</span>
					</div>

					<div className="multi-equation">
						<Equation inline className="equation" math={"`logP_(calc)^(BLM) = 1.063 \\ logP_(\Sigma)^(BLM) + 3.669`"}/>
						<span className="equation-number">(4)</span>
					</div>

					<div className="multi-equation">
						<Equation inline className="equation" math={"`logP_(0 \\ calc)^(PAMPA-DS) = 0.981 \\ logP_(\Sigma)^(BLM) + 2.159`"}/>
						<span className="equation-number">(5)</span>
					</div>

					<div className="multi-equation">
						<Equation inline className="equation" math={"`logP_(0 \\ calc)^(BBB) = 0.375 \\ logP_(\Sigma)^(BLM) - 1.600`"}/>
						<span className="equation-number">(6)</span>
					</div>

					<div className="multi-equation">
						<Equation inline className="equation" math={"`logP_(0 \\ calc)^(Caco-2 // MDCK) = 0.272 \\ logP_(\Sigma)^(BLM) - 2.541`"}/>
						<span className="equation-number">(7)</span>
					</div>

					<p>
						where <i>ASA</i> is the accessible surface area of the
						molecule, <Equation inline math={"`logP_(\Sigma)^(BLM)`"}/> is the integral of the
						Gibbs free energy of the molecule over the hydrophobic thickness <i>d</i> of
						the DOPC bilayer (from -15 Å to +15 Å from the membrane center).
					</p>
				</div>
			</div>
		)
	}
}

//
export default About;
