// import React from 'react';
import React, { Component } from 'react'
import './Acknowledgments.scss';
import ScrollableAnchor from 'react-scrollable-anchor'

class Acknowledgments extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {}
	}

	render()
	{
		return(
			<div className="acknowledgments">
				<div>
                    <div className="page-header">
                        Acknowledgments
                    </div>

					<div className="ack-text">
					
					<b><h1>PerMM database was developed using the following databases and servers:</h1></b>
						<ul>
							<li><a href="https://pubchem.ncbi.nlm.nih.gov/" target="_blank">PubChem</a>, the compound database</li>
							<li><a href="https://www.drugbank.ca/" target="_blank">DrugBank</a>, the comprehensive resource on FDA-approved and experimental drugs</li>
							<li><a href="http://www.genome.jp/kegg/kegg1.html" target="_blank">KEGG</a>, Kyoto Encyclopedia of Genes and Genomes (genomic, chemical, and functional information)</li>
							<li><a href="https://www.ebi.ac.uk/chebi/" target="_blank">ChEBI</a>, Chemical Entities of Biological Interest</li>
							<li><a href="https://www.pharmgkb.org/" target="_blank">PharmGKB</a>, knowledge about the impact of genetic variation on drug response</li>
							<li><a href="http://www.hmdb.ca/" target="_blank">HMDB</a>, the Human Metabolome Database, information about small methabolites in human body</li>
							<li><a href="http://lmmd.ecust.edu.cn/admetsar1/" target="_blank">admetSAR</a>, tool and resource for evaluating chemical ADMET properties</li>
							<li><a href="http://www.rcsb.org/pdb/" target="_blank">PDB</a>, the Protein Data Bank, database of protein 3D structures</li>
						</ul>

						<p>
							PerMM was developed using NACCESS, a program for calculating solvent accessible surface areas of proteins, Hubbard S.J., Thornton J.M. (1993),  Department of Biochemistry and Molecular Biology , University College London, UK.
						</p>

						<h1>Funding</h1>
						<p>
						<a href="https://projectreporter.nih.gov/project_info_details.cfm?aid=9243005&amp;icde=33995486&amp;ddparam=&amp;ddvalue=&amp;ddsub=&amp;cr=1&amp;csb=default&amp;cs=ASC&amp;pball=" target="_blank">The National Institute of Drug Abuse of the National Institute of Health</a>
						</p>						
						
						<h1>Images</h1>
						<p>
							<a href="https://commons.wikimedia.org/wiki/File:Blausen_0213_CellularDiffusion.png" target="_blank">Wikimedia Common File:Blausen 0213 CellularDiffusion.png</a>
						</p>
					</div>
				</div>
			</div>
		)
	}
}

export default Acknowledgments;
