import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";
import Config, { ComputationServer } from "../../Config.js";
import ApiSearch from "../api_search/ApiSearch.js";
import Popup from "./popup/Popup.js";
import "./Server.scss";

type PermmProps = {
    computationServer: ComputationServer,
};

function getInitialState() {
	return {
		loading: false,
		instructions: false,
		submitted: false,
		waitingUrl: "",
		errors: [],
		pdbSearchData: undefined,
		pdbSearchLoading: false,
		pdbSearchMolcode: "",

		temperature: 298,
		ph: 7.4,
		optimizationMethod: "DRAG",
		estimatePermeabilityPo: false,
		fileInputType: "upload",
		pdbFile: undefined,
		userEmail: ""
	}
};

class Server extends React.Component<PermmProps> {
	constructor(props) {
		super(props);

		this.state = getInitialState();

		this.handleChange = (event) => {
			let newstate = {};

			if (this.state.errors.length > 0) {
				newstate.errors = [];
			}

			if (event.target.name === "pdbFile") {
				newstate.pdbFile = event.target.files[0]
			}
			else {
				newstate[event.target.name] = event.target.value;
			}

			this.setState(newstate);
		}

		this.onPdbSearch = () => {
			this.setState({ pdbSearchLoading: true });
		}
		this.onPDBSearchComplete = (data) => {
			const selectedMolcode = data.objects.length > 0 ? data.objects[0].molecule_code : "";
			this.setState({
				pdbSearchData: data.objects,
				pdbSearchLoading: false,
				pdbSearchMolcode: selectedMolcode
			});
		}

		this.reset = () => {
			this.setState(getInitialState());
		}

		this.validEmail = (email) => {
			if (email === "") {
				return true;
			}
			const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return emailPattern.test(String(email).toLowerCase());
		}

		this.filenameValid = (filename) => {
			if (filename.indexOf(".") < 0) {
				return false;
			}
			const parts = filename.split(".");
			return parts[parts.length - 1].toLowerCase() === "pdb";
		}

		this.phValid = (ph) => {
			return !isNaN(ph) && parseFloat(ph) >= 0 && parseFloat(ph) <= 14;
		}

		this.tempValid = (temp) => {
			return !isNaN(temp) && parseFloat(temp) >= 0;
		}

		this.sendData = (data) => {
            const { computationServer } = this.props;
			axios.post(Config.computation_servers[computationServer] + "permm", data)
				.then(res => {
					this.setState({
						loading: false,
						waitingUrl: res.data.waitingUrl,
						submitted: true
					})
				})
				.catch((err) => {
	                this.setState({
	                    errors: [{
							key: 0,
							text: "An error occurred when submitting: " + JSON.stringify(err)
						}],
	                    loading: false,
	                });
	            });
		}

		this.handleSubmit = (event) => {
			event.preventDefault();
			let errors = [];

			if (this.state.fileInputType === "upload") {
				if (!this.state.pdbFile || !this.filenameValid(this.state.pdbFile.name)) {
					errors.push({
						key: errors.length,
						text: "Invalid File Format - Please submit a .pdb file"
					});
				}
			}
			else if (this.state.fileInputType === "search") {
				if (!this.state.pdbSearchMolcode) {
					errors.push({
						key: errors.length,
						text: "No .pdb file selected - Please select a .pdb file from search results"
					});
				}
			}

			if (!this.tempValid(this.state.temperature)) {
				errors.push({
					key: errors.length,
					text: "Invalid Temperature - Please choose a value greater than or equal to 0"
				});
			}
			if (!this.phValid(this.state.ph)) {
				errors.push({
					key: errors.length,
					text: "Invalid pH - Please choose a value between 0 and 14"
				});
			}
			if (!this.validEmail(this.state.userEmail)) {
				errors.push({
					key: errors.length,
					text: "Invalid Email"
				});
			}
			if (errors.length > 0) {
				this.setState({ errors });
				return;
			}

			let formData = new FormData();
			formData.append("temperature", this.state.temperature);
			formData.append("ph", this.state.ph);
			formData.append("optimizationMethod", this.state.optimizationMethod);
			formData.append("estimatePermeabilityPo", this.state.estimatePermeabilityPo);

			if (this.state.userEmail !== "") {
				formData.append("userEmail", this.state.userEmail);
			}

			if (this.state.fileInputType === "upload") {
				formData.append("pdbFile", this.state.pdbFile);
				this.sendData(formData);
			}
			else {
				const url = Config.pdb_source_file(this.state.pdbSearchMolcode);
				axios.get(url, { responseType: "arraybuffer" })
					.then((res) => {
						const fileBlob = new Blob([res.data]);
						const filename = this.state.pdbSearchMolcode + ".pdb";
						formData.append("pdbFile", fileBlob, filename);
						this.sendData(formData);
					});
			}
			this.setState({ loading: true });
		}
	}

	renderPdbFileSearchResults() {
		if (this.state.pdbSearchLoading) {
			return <img src={Config.loading_image_small} alt="Loading" />;
		}

		if (this.state.pdbSearchData === undefined) {
			return null;
		}

		if (this.state.pdbSearchData.length === 0) {
			return <span>No results found.</span>;
		}

		let items = [];
		for (let i = 0; i < this.state.pdbSearchData.length; ++i) {
			items.push(
				<option value={this.state.pdbSearchData[i].molecule_code} key={i}>
					{this.state.pdbSearchData[i].molecule_code}.pdb
				</option>
			);
		}

		return (
			<select
				className="pdb-search-results"
				onChange={(e) => this.setState({ pdbSearchMolcode: e.target.value })}
			>
				{items}
			</select>
		);
	}


	render() {
        const { computationServer } = this.props;
		// loading image after submit
		if (this.state.loading) {
			return (
				<div className="info-loading">
					<img className="loading-image" src={Config.loading_image} />
				</div>
			);
		}

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;
        const useOtherServer = computationServer == "memprot"
            ? <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/permm_server_cgopm">cgopm-based</Link> version
            </div>
            : <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/server">AWS-based</Link> version
            </div>;

		// screen with form to submit
		if (!this.state.submitted) {
			return (
				<div className="server">
					<form onSubmit={this.handleSubmit}>
						<ul>
							<li className="title">
								<div className="page-header">Calculate Transmembrane Translocation Pathway ({serverType})</div>
								<p>
									PerMM web server implements our computational method for calculation of binding affinities of molecules to membranes,
									energy barriers along the membrane normal, and permeability coefficients. The server was developed to assist
									investigators at early stages of drug development in optimization of cell permeability of new therapeutics derived
									from natural products.
								</p>
							</li>

							<li>
								<div id="pdb_upload_input">
									<input
										type="radio"
										name="fileInputType"
										id="upload_choice"
										className="no-indent"
										value="upload"
										checked={this.state.fileInputType === "upload"}
										onChange={this.handleChange}
									/>{" "}
									<label className="description" htmlFor="file">
										Upload your PDB file (please use extension .pdb):
									</label>{" "}
									<input
										className={this.state.fileInputType === "upload" ? "" : "hidden"}
										type="file"
										name="pdbFile"
										size="7000000"
										onChange={this.handleChange}
									/>
									<div>
										One can use downloadable "Source" PDB files provided by PerMM database, for
										example, <Link to="/molecules/4">acetamide</Link>.
									</div>
									<input
										type="radio"
										name="fileInputType"
										id="search_choice"
										className="no-indent"
										value="search"
										checked={this.state.fileInputType === "search"}
										onChange={this.handleChange}
									/>{" "}
									<label className="description" htmlFor="file">
										Search for PDB file in PerMM
									</label>{" "}
									<div className={this.state.fileInputType === "search" ? "pdb-file-search" : "hidden"}>
										<ApiSearch
											className="no-indent"
											onSearch={this.onPdbSearch}
											onSearchComplete={this.onPDBSearchComplete}
											searchBy="name"
										/>
										{this.renderPdbFileSearchResults()}
									</div>
								</div>
							</li>

							<li id="li_4">
								<label className="description" htmlFor="element_11">Experimental conditions <div className="desc"> </div> </label>
								<br />
								<table>
									<tbody>
										<tr>
											<td>
												<label htmlFor="element_4">T° (K): </label>
											</td>
											<td>
												<input id="temp" name="temperature" className="element text small" type="text" maxLength="255" value={this.state.temperature} onChange={this.handleChange} />
											</td>
										</tr>
										<tr>
											<td>
												<label htmlFor="element_6">pH: </label>
											</td>
											<td>
												<input id="pHout" name="ph" className="element text small" type="text" maxLength="255" value={this.state.ph} onChange={this.handleChange} />
											</td>
										</tr>
									</tbody>
								</table>
							</li>

							<li id="li_2">
								<label className="description" htmlFor="element_11">Optimization method for calculation of transmembrane pathway:
									<div className="description" htmlFor="element_9"></div>
								</label>

								<span>
									<select id="optMethod" name="optimizationMethod" onChange={this.handleChange}>
										<option value="DRAG">"Drag" optimization</option>
										<option value="GLOBAL">Global rotational optimization for each Z value</option>
									</select>
								</span>
							</li>

							<li id="li_3">
								<input
									type="radio"
									checked={this.state.estimatePermeabilityPo === false}
									onChange={() => this.setState({ estimatePermeabilityPo: false })}
								/>
								{" "}Estimate BLM permeability including deionization energy for ionizable molecules
								<br />
								<input
									type="radio"
									checked={this.state.estimatePermeabilityPo === true}
									onChange={() => this.setState({ estimatePermeabilityPo: true })}
								/>
								{" "}Estimate intrinsic permeability Po for BLM, BBB, and Caco-2 (ionizable molecules are assumed to be uncharged in water)
							</li>

							<li id="li_4">
								<label htmlFor="element_11">
									Send report to (optional):
								</label>
								&nbsp;
								<input id="email" name="userEmail" className="element text small" type="email" placeholder="email address" maxLength="255" autoComplete="email" value={this.state.userEmail} onChange={this.handleChange} />
							</li>

							{this.state.errors.map(err => <li id="err" key={err.key}> {err.text} </li>)}

							<li className="buttons">
								<input type="submit" name="submit" value="Submit" />
							</li>
							<div
								className="expand-link"
								onClick={() => this.setState({ instructions: !this.state.instructions })}
							>
								{(this.state.instructions ? "hide" : "show") + " instructions"}
							</div>
							{this.state.instructions ? <Popup /> : ""}
						</ul>
					</form>
					{useOtherServer}
				</div>
			);
		}

		// submit successful screen (this.state.submitted && !this.state.loading)
		else {
			return (
				<div className="server">
					<ul>
						<li className="title">
							<h3>Your job has been submitted</h3>
						</li>
						<li>
							<p className={this.state.userEmail === "" ? "hidden" : ""}>
								You will be notified at <a href={"mailto:" + this.state.userEmail}>{this.state.userEmail}</a> when the calculations are complete.
							</p>
						</li>
						<li>
							<p>
								Access your <a target="_blank" className="external" href={this.state.waitingUrl}>Results</a>
							</p>
						</li>
						<li>
							<button className="submit" onClick={this.reset}>Submit Again</button>
						</li>
					</ul>
				</div>
			);
		}


	}
}

export default Server;
