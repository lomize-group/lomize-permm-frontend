import React from 'react';

import { Link } from 'react-router-dom';

import './Popup.scss';

class Popup extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}


	render()
	{
		return(
			<div className="popup">
				<div>
					<h1>Input</h1>
					<p>
						To calculate the transmembrane translocation pathway and permeability coefficient,
						please define the temperature (in K) and pH and upload the coordinate file of a compound.
						<br/>
						<br/>
						<strong>Two optimization options</strong> can be defined by user for calculation of the lowest energy pathway ∆G(z) of the molecule across the membrane:
						<br/>
						<br/>
						<ol className="letter-list">
							<li>
								<span className="underline">Drag method (recommended).</span> The
								molecule is dragged across the membrane with local energy minimization with respect to rotational
								variables of the molecule in every point z +∆z, starting from the optimal rotational orientation found
								in the previous point z. This method produces an asymmetric energy curve and allows more precise
								estimation of the transmembrane energy barrier, especially for large amphiphilic molecules.
							</li>
							<li>
								<span className="underline">Global energy optimization</span> of
								rotational orientation of the molecule in each position z along the bilayer normal.
								This option produces symmetric energy curve and can be used for small molecules. However,
								it neglects the free energy barrier for the flip-flop of the molecule in the middle of the membrane.
							</li>
						</ol>
						<br/>
						<strong>An option for ionizable compounds. </strong>
						The box at the bottom allows choosing an option ("yes") to assume an ionizable compund is uncharged in water.
						This option should be used to calculate the "intrinsic" BBB and Caco-2 permeability coefficients (logPo) or
						make comparisons with experimental BLM data provided for uncharged forms of ionizable molecules. Calculations
						with "no" option include energy of deionization of the molecule when it is transferred from water to nonpolar
						interior of the lipid bilayer. This energy depends on the difference of pH and pKa of ionizable groups.
						<br/>
						<br/>
						<strong>Input coordinate file </strong>
						must be prepared in the PDB format. The coordinate files for peptides and small molecules can be downloaded
						from the Protein Data Bank, the Cambridge Structural Database, PubChem, DrugBank, or generated computationally
						using a variety of molecular modeling software (i.e. Chem3D). Converting SDF to PDB formats can be done by
						several programs, including PyMOL. One can also use "Source" coordinate files provided by our PerMM database
						(they are downloadable from pages for individual molecules included in the database, or as a single .tar file
						from PerMM download page).
						<br/>
						<br/>
						<span className="underline">Multiple conformations</span> of the molecules can be included as several
						models (MODEL 1, etc.; see "Source" file for Tol-Gly-Gly in PerMM database, for example). Lowest
						transfer energy conformer is selected automatically for vizualization in every point z of the curve,
						but the energy for calculating the permeability coefficient is averaged for the conformational ensemble.
						<br/>
						<br/>
						<span className="underline">
							Two slightly different versions of input PDB format are used for peptides and small molecules.
							The version for peptides
						</span> uses
						standard PDB format. Hydrogen atoms can be included, but not required. Only all non-hydrogen atoms must
						be present in the file. The parameters of amino acid residues are taken from a library that includes all
						standard L- and D- residues, L-ornithine, D-Pen and a number of others. Any molecule with more than one residue
						indicated in standard PDB RESIDUE field will be automatically interpreted as a "peptide".
						<br/>
						<br/>
						<span className="underline">The coordinate files for small molecules</span> must include all hydrogens, provide
						different names for all non-hydrogen atoms (e.g. N1,N2,N3...), and include pKa of all groups that are ionized
						within the pH values of interest (for example, 6.0 to 8.0). Small molecules must have the same residue number in
						RESIDUE field for all its atoms. A peptide can be defined as a small molecule by assigning the same residue
						number to all atoms, making sure that all non-hydrogen atoms have different names, and by including the pKa values
						for ionizable molecules.  Using a PDB file without any pKa values will produce result for the unionized form. The
						pKa values must be assigned to non-hydrogen atoms of the corresponding ionizable group, such as O of COO- group
						or N of NH3+ group, and be included at the beginning of the file as REMARK PKA records (see example below and
						"Source" files of ionizable molecules included in PerMM database).
						<br/>
						<br/>
						<table className="example-table">
							<tbody>
								<tr>
									<td>REMARK</td>
									<td>PKA</td>
									<td>O1</td>
									<td>0</td>
									<td>1.00</td>
									<td>3.47</td>
									<td>-1</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>1</td>
									<td>C</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-0.116</td>
									<td>0.604</td>
									<td>0.376</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>2</td>
									<td>C1</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-0.806</td>
									<td>-0.566</td>
									<td>0.049</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>3</td>
									<td>C2</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-0.751</td>
									<td>1.838</td>
									<td>0.405</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>4</td>
									<td>C3</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-2.163</td>
									<td>-0.646</td>
									<td>-0.261</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>5</td>
									<td>C4</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-2.101</td>
									<td>1.919</td>
									<td>0.095</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>6</td>
									<td>C5</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-2.084</td>
									<td>0.768</td>
									<td>-0.238</td>
									
								</tr>
								<tr>
									<td>ATOM</td>
									<td>7</td>
									<td>C6</td>
									<td>ACETA</td>
									<td>0</td>
									<td>-0.137</td>
									<td>-1.977</td>
									<td>0.028</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>8</td>
									<td>C7</td>
									<td>ACETA</td>
									<td>0</td>
									<td>2.137</td>
									<td>0.727</td>
									<td>-0.281</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>9</td>
									<td>C8</td>
									<td>ACETA</td>
									<td>0</td>
									<td>3.583</td>
									<td>0.610</td>
									<td>0.202</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>10</td>
									<td>O</td>
									<td>ACETA</td>
									<td>0</td>
									<td>1.252</td>
									<td>0.561</td>
									<td>0.691</td>
								</tr>
							</tbody>
						</table>
						<br/>
						In this example, pKa value is assigned to one of oxygens in a COO- group; “O1” is the name of the atom to which
						the pKa value was assigned, “0” is the number of amino acid residue to which this atom belongs in the pdb file;
						“1.0” indicates that pKa was assigned to the single specified atom, rather than being distributed between several
						ionizable atoms (as a slightly better approximation, the user can include two O atoms of COO- group with weights
							0.5 and 0.5, three nitrogens of guanidine group with weights of 0.33, etc.); “3.47” is the pKa value; and ”-1”
						is the sign of the charge at neutral pH (negatively charged group).
						<br/>
						<br/>
						The remark for an NH3+ group could be, for example,
						<br/>
						<br/>
						<table className="example-table">
							<tbody>
								<tr>
									<td>REMARK</td>
									<td>PKA</td>
									<td>N1</td>
									<td>0</td>
									<td>1.00</td>
									<td>9.00</td>
									<td>1</td>
								</tr>
							</tbody>
						</table>
						<br/>
						For ionized groups with delocalized charge we suggest assigning the pKa value to several atoms of the group,
						as described above (i.e. using weights of 0.33 for each of three atoms in a group, for example).
						PerMM was not tested and parameterized for reproducing permeability of permanently charged hydrophobic ions.
						For example, one can assign a very high pKa value for hydrophobic cations (e.g. 20) in input file, so they will
						remain charged even in the lipid bilayer.
						<br/>
						Importantly, the PDB format uses a position-dependent input, i.e. right-justified fields for numbers and left-justified
						fields for text. Hence the empty spaces in REMARK record are important and should be used (as in the example above).
						<br/>
						<br/>
						<span className="underline">Dipole moments of polar groups are defined automatically</span> using a library of dipole
						moments for different chemical groups. Dipole moments are assigned to specific chemical groups, rather than to whole
						molecules. For example, a single dipole moment of Trp aromatic ring is automatically assigned to its N atom. A user
						can redefine group dipole moments manually (based on experimental or quantum chemistry-based values), which may improve
						the precision of calulations, especially for molecules with complex heterocyclic rings. Group dipole moments can be
						redefined by including the following records in the beginning of the PDB file (see example below or the source file for
						2,3-Dideoxyadenosine in PerMM database). Note that the underlying PPM model was not parameterized for hydrophobic ions.
						<br/>
						<br/>
						<table className="example-table">
							<tbody>
								<tr>
									<td>REMARK</td>
									<td>DIP</td>
									<td>N4</td>
									<td>1</td>
									<td>3.0</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>1</td>
									<td></td>
									<td>ADEN</td>
									<td>1</td>
									<td>-1.231</td>
									<td>-1.450</td>
									<td>0.000</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>2</td>
									<td>N1</td>
									<td>ADEN</td>
									<td>1</td>
									<td>-2.074</td>
									<td>0.637</td>
									<td>0.000</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>3</td>
									<td>N2</td>
									<td>ADEN</td>
									<td>1</td>
									<td>0.090</td>
									<td>1.831</td>
									<td>0.000</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>4</td>
									<td>N3</td>
									<td>ADEN</td>
									<td>1</td>
									<td>1.992</td>
									<td>0.396</td>
									<td>0.000</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>5</td>
									<td>N4</td>
									<td>ADEN</td>
									<td>1</td>
									<td>1.806</td>
									<td>-1.912</td>
									<td>0.000</td>
								</tr>
								<tr>
									<td>ATOM</td>
									<td>6</td>
									<td>C</td>
									<td>ADEN</td>
									<td>1</td>
									<td>-0.162</td>
									<td>-0.570</td>
									<td>0.000</td>
								</tr>
							</tbody>
						</table>
						<br/>
						In this example, “N4” in the REMARK record is the name of the polar atom to which the dipole moment was assigned; “1” is the number of the “amino acid residue” where this atom belongs; and “3.00” is the group dipole moment (in D).
					</p>
				</div>

				<div>
					<h1>Output</h1>
					<p>
						The calculations take 1 to 4 minutes, depending on the number of conformatons included in the input coordinate file and the size of the molecule.
						<br/>
						<br/>
						Output of the server includes the following:
						<br/>
						<br/>
						<ol>
							<li>	
								Graphical representation of the calculated transfer energy curve, ∆G (z)
							</li>
							<li>
								Logarithms of permeability coefficients that are calculated for DOPC bilayer
								and estimated for plasma membrane, blood-brain barrier (BBB) and Caco-2 cells.
							</li>
							<li>
								Interactive 3D visual images of a given compound along the translocation pathway ("Pathway" link) and the compound
								bound to the lipid bilayer ("Bound state" link).The dots in images (DUMMY atoms in the coordinate files) represent
								hydrocarbon core boundaries of the lipid bilayer; lipid headgroups are located outside these boundaries.
							</li>
							<li>
								Downloadable output coordinate file XXXout.pdb that provides multiple positions of the molecule during its movement
								across the lipid bilayer. They are included as multiple models. Second output coordinate file (XXX1.pdb) provides
								membrane-bound state of the molecule. Only the lowest transfer energy conformer in each position was included in the
								output file.
							</li>
							<li>
								Output messages including the calculated permeability coefficients and ∆G(z) curve. The curve also shows the number
								of the lowest energy conformer selected in every point of the curve.
							</li>
						</ol>
					</p>
				</div>

				<div>
					<h1>Interpretation of Results</h1>
					<div>
						<ol>
							<li>
								The permeability coefficient is calculated for a monomeric molecule assumed to be completely
								soluble in water. The results are dependent on pH and temperature.
							</li>
							<li>Intrinsic permeability coefficients, logP<sub>0</sub>, are calculated for BLM (DOPC bilayer) and roughly estimated for
								plasma membrane (PM), PAMPA-DS system, blood-brain barrier (BBB) and Caco-2/MDCK cell membranes. A significant deviation
								of values for BBB and Caco-2 from values for artificial lipid bilayers (BLM) may be caused by contributions
								of facilitated diffusion, carrier-mediated transport, active efflux or binding to proteins in living cells.
								According to our estimates, intrinsic permeability coefficient for BBB, LogP > -4.35 for BBB indicates that the
								compound can passively traverse the blood-brain barrier and, therefore, is expected to be CNS-positive,
								unless it is a substrate for efflux transporters, such as P-glycoprotein or other ABC-transporters.
							</li>
							<li>"Pathway" GLMol link provides the interactive 3D images of a compound moving across the membrane.
								The pathway can be also visualized via PyMOL using “load XXXout.pdb, multiplex=1” command for the
								output coordinate file.
							</li>
							<li>The calculated permeability coefficients of DOPC bilayer show a good correlation with the corresponding
								experimental values for BLM (R2=0.88 for 127 compounds), but more poor correlation with intrinsic permeability
								coefficients determined in BBB and Caco-2 cell-based assays (R2 are 0.69 and 0.53 for the sets of 182 and 165
								compounds, respectively).
							</li>
						</ol>
					</div>
				</div>

				<div id="contact">
					<h1>Contact</h1> 
					<p>
						Please send any questions or requests to Andrei Lomize (
						<a href="mailto:almz@umich.edu">almz@umich.edu</a>
						)
					</p>
				</div>

                <div>
                <h1>Source code</h1>
                    <p>PerMM&nbsp;
                        <a
                            href="https://console.cloud.google.com/storage/browser/permm-assets/permm_code"
                            target="_blank"
                        >
                            source code
                        </a>
                    </p>
                </div>
			</div>
		);
	}
}

export default Popup;
