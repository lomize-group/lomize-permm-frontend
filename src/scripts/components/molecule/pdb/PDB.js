import React from 'react';

import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import Config from '../../../Config.js';

import ImgWrapper from '../../../helpers/img_wrapper/ImgWrapper.js';

import './PDB.scss';

class PDB extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}

	render()
	{
		return(
			<div className="pdb">
				<strong>{this.props.name}</strong>
				<a href={Config.pubchem_image(this.props.molid)}>
					<ImgWrapper 
						className="img-style" 
						src={Config.pubchem_image(this.props.molid)} 
					/>
				</a>
				<a href={Config.ppm_image(this.props.molid)}>
					<ImgWrapper 
						className="img-style" 
						src={Config.ppm_image(this.props.molid)} 
					/>
				</a>
				<hr className="divider"/>
				<div className="topology-section">
					<div className="molecule-link">
						<div className="3d-viwer">
							<p>
								3D View: <Link to={{pathname: "/molecules/glmol/"+this.props.id+"/"+this.props.molid }}>Pathway</Link>
								{" or "}
								<a className="external" href={"https://bioinformatics.org/firstglance/fgij//fg.htm?mol=" + Config.pdb_bound_state_file(this.props.molid)}>Bound state</a>
							</p>
						</div>
						<hr className="divider"/>
						<p>Download Coordinates:</p>
						<p>
							<a href={Config.pdb_pathway_file(this.props.molid)} download>Pathway</a>
							, <a href={Config.pdb_bound_state_file(this.props.molid)} download>Bound State</a>
							, or <a href={Config.pdb_source_file(this.props.molid)} download>Source</a>
						</p>
					</div>
				</div>
				
			</div>
		);
	}
}

PDB.propTypes = {
	molid: PropTypes.string.isRequired,
	id: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
};

export default PDB;
