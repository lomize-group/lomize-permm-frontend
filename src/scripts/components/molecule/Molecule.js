import React from 'react';
import PropTypes from 'prop-types';
import Config from '../../Config.js';
import PDB from './pdb/PDB.js';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Info from '../info/Info.js';
import MoleculeViewer from '../molecule-viewer/MoleculeViewer.js';

import './Molecule.scss';

class Molecule extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
		this.state.has_data = false;
		this.state.id = props.currentUrl.params.id;

		axios.get(Config.baseUrl + Config.classification.molecules.api.route + '/' + props.currentUrl.params.id)
		.then((res) => {
			let newstate = {
				data: res.data,
				has_data: true,
				info: {}
			}
			this.setState(newstate);
		});

		this.twoDigits = (num) => {
			return num == null ? null : Number.parseFloat(num).toFixed(2);
		}

		this.oneDigit = (num) => {
			return num == null ? null : Number.parseFloat(num).toFixed(1);
		}

		this.render_description = () => {
			if (this.state.data.annotation) {
				return(<p key="description"><span className="green italic">Description: </span>{this.state.data.annotation}</p>);	
			}
		}

		this.render_info = () => {
			return(
				<div className='molecule-info'>
					<ul>
						<li><span className="green italic">Class: </span><Link to={'/classes/'+this.state.data.class_type_id}>{this.state.data.class_type.name}</Link></li>
						<li><span className="green italic">Group: </span><Link to={'/groups/'+this.state.data.group_id}>{this.state.data.group.name}</Link></li>
						<li><span className="green italic">Molecular Weight (g/mol): </span>{this.state.data.molecular_weight}</li>
						<li><span className="green italic">pKa: </span>{this.oneDigit(this.state.data.pKa)}</li>
					</ul>
				</div>
				)
		}

		this.renderTableRow = (right, left, attr) => {
			return(
				<tr className={attr}>
					<td className="right"><b>{right}</b></td>
					<td className="left">{left}</td>
				</tr>
			)
		}

		this.render_measurements = (name, measurements) => {
			let items = [];
			for (let i = 0; i < measurements.length; i++) {
				items.push(
					<span key={i}>
						{this.twoDigits(measurements[i].log_perm_coeff)}
						{
							name === "BLM" && 
							" (" + measurements[i].charged_state + ") "
						}
						{
							name === "BLM" &&
							<span>
								{" "}
								<a href={Config.pubmed(measurements[i].pubmed)} target="_blank">
									Pubmed
								</a>
							</span>
						}
						{i + 1 < measurements.length && ", "}
					</span>
				);
			}
			return items;
		}

		this.render_exp_permeability = () => {
			let rows = [];
			let headers = this.state.data.membrane_headers;
			let next_key = 0;

			for (let i = 0; i < headers.length; i++) {
				let header = headers[i];
				let vals = this.state.data.experimental_measurements[header.id];
				if (vals) {
					rows.push(
						<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
							<td className="right">
								<b>logP<sub>0 {header.name}</sub></b>
							</td>
							<td className="left">
								{this.render_measurements(header.name, vals)}
							</td>
						</tr>
					);
					++next_key;
				}
			}

			return(
				<div className="info-table">
					<table>
						<tbody>
						<tr className="dark">
							<th colSpan={2}>Permeability Parameters (experimental)</th>
						</tr>
						{rows}
						</tbody>
					</table>
				</div>
			);
		}

		this.render_calc_permeability = () => {
			let rows = [];
			let next_key = 0;
			
			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>calc</sub> BLM (ionized in water)</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp7_4_blm)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>0,calc</sub> BLM (neutral form)</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp0_blm)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logPo PAMPA-DS</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp0_pampa_ds)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>m-6.5,calc</sub> PAMPA-DS</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp6_5_pampa_ds)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>m-7.4,calc</sub> PAMPA-DS</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp7_4_pampa_ds)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>0,calc</sub> Plasma Membrane</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp0_pm)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>0,calc</sub> Caco-2 (P<sub>0</sub>)</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp0_caco)}</td>
				</tr>
			);
			++next_key;

			rows.push(
				<tr key={next_key} className={next_key % 2 == 0 ? "light" : "dark"}>
					<td className="right"><b>logP<sub>0,calc</sub> BBB (P<sub>0</sub>)</b></td>
					<td className="left">{this.twoDigits(this.state.data.logp0_bbb)}</td>
				</tr>
			);

			return(
				<div className="info-table">
					<table>
						<tbody>
						<tr className="dark">
							<th colSpan={2}>Permeability Parameters (calculated)</th>
						</tr>
						{rows}
						</tbody>
					</table>
				</div>
			);
		}
	}


	
	
	changeImage(new_pdbid){
		this.setState({image_pdbid : new_pdbid});
	}

	renderSimplePDBLink(pdbid, showComma){
		if (this.state.image_pdbid == pdbid) {
			return(
				<span className="cursor-hand" key={pdbid}>
					<strong>
						{pdbid}
					</strong>
					{showComma ? ", " : ""}
				</span>
			);	
		}
		else {
			return(
				<span className="cursor-hand" key={pdbid}>
					<a onClick={() => this.changeImage(pdbid)}>
						{pdbid}
					</a>
					{showComma ? ", " : ""}
				</span>
			);
		}
	}
	renderOtherPDBs(pdbid){
		let items = []
		let sec_structures = this.state.representative_structures.length - 1;
		let sec_structures_seen = 0;
		for (let i = 0; i < this.state.representative_structures.length; i++) {
			let struct = this.state.representative_structures[i];
			if (struct.pdbid != pdbid) {
				sec_structures_seen++;
				let showComma = sec_structures_seen !== sec_structures;
				items.push(this.renderSimplePDBLink(struct.pdbid, showComma));
			}
		}
		return items;
	}


	render()
	{
		if (!this.state.has_data) {
			return(
				<div className="info-loading">
					<img className="loading-image" src={Config.loading_image}/>
				</div>
			);
		}
		return(
			<div className="molecule">
				<div className="page-header">
					{this.state.data.name}
				</div>
				{this.render_info()}
				<div className="random-molecule">
					<PDB molid={(this.state.data.molecule_code)} id={this.state.data.id} name={this.state.data.name}/>
				</div>
				{this.render_exp_permeability()}
				{this.render_calc_permeability()}
				<div className="info-table">
					<table>
						<tbody>
						<tr className="dark"><th colSpan="2">{"Additional Information"}</th></tr>
							<tr className="light">
								<td className="right"><b>ΔG<sub>bind</sub>(kcal/mol)</b></td>
								<td className="left">{this.oneDigit(this.state.data.membrane_binding_energy)}</td>
							</tr>
							{this.renderTableRow("Influx Transporters", this.state.data.influx, "dark")}
							{this.renderTableRow("Efflux Transporters", this.state.data.efflux, "light")}
							{this.renderTableRow("Localization", this.state.data.intracellular_localization, "dark")}
						</tbody>
					</table>
				</div>
				<div className="info-table">
					<table>
						<tbody>
						<tr className="dark"><th colSpan="2">{"Links to External Resources"}</th></tr>
							{this.renderTableRow("PubChem", 
												this.state.data.pubchem
												? <a className={this.state.data.pubchem === "" ? "" : "external"}
												href={Config.classification.molecules.pubchem(this.state.data.pubchem)} target="_blank">
													{this.state.data.pubchem}
												</a>
												: "",
												"light" )
							}
							{this.renderTableRow("DrugBank", 
												<a className={this.state.data.drugbank === "" ? "" : "external"}
												href={Config.classification.molecules.drugbank(this.state.data.drugbank)} target="_blank">
													{this.state.data.drugbank}
												</a>, 
												"dark" )}
							{this.renderTableRow("KEGG", 
												<a className={this.state.data.KEGG === "" ? "" : "external"}
												href={Config.classification.molecules.kegg(this.state.data.KEGG)} target="_blank">
													{this.state.data.KEGG}
												</a>, 
												"light" )}
							{this.renderTableRow("ChEBI", 
												this.state.data.chEBI
												? <a className={this.state.data.chEBI === "" ? "" : "external"}
												href={Config.classification.molecules.chebi(this.state.data.chEBI)} target="_blank">
													{this.state.data.chEBI}
												</a>
												: "", 
												"dark" )}
							{this.renderTableRow("Wikipedia", 
												<a className={this.state.data.wiki_name === "" ? "" : "external"}
												href={Config.classification.molecules.wikipedia(this.state.data.wiki_name)} target="_blank">
													{this.state.data.wiki_name}
												</a>, 
												"light" )}
							{this.renderTableRow("Human Metabolome", 
												<a className={this.state.data.metabolome === "" ? "" : "external"}
												href={Config.classification.molecules.metabolome(this.state.data.metabolome)} target="_blank">
													{this.state.data.metabolome}
												</a>, 
												"dark" )}
							{this.renderTableRow("PDB", 
												<a className={this.state.data.PDB === "" ? "" : "external"}
												href={Config.classification.molecules.pdb(this.state.data.PDB)} target="_blank">
													{this.state.data.PDB}
												</a>, 
												"light" )}						
						</tbody>
					</table>
				</div>
				{this.render_description()}

			</div>
		);
	}
}


function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(Molecule);
