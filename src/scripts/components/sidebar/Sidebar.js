import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';

import { loadStats } from '../../actions/stats.js';

import ImgWrapper from '../../helpers/img_wrapper/ImgWrapper.js';

import Config from '../../Config.js';

import axios from 'axios';

import './Sidebar.scss'

class Sidebar extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			loading: true,
		};
		if (props.stats === ""){
			axios.get(Config.baseUrl + '/stats').then((res) => {
				props.loadStats({
					groups: res.data.table.groups,
					classes: res.data.table.class_types,
					membrane_systems: res.data.table.membrane_systems,
					molecules: res.data.table.molecules
				});

				this.setState({
					loading: false,
				});
			});
		}
		else{
			this.state = {
				loading: false,
			}
		}

	}


	navitem(key, name, route, subclasses, is_selected){
		if (!is_selected) {
			return (
				<div key={key} className={"item not-selected"}>
					<Link to={route}><strong>{name}</strong></Link>
					<span>&nbsp;({subclasses})</span>
				</div>
			);
		}
		else {
			return (
				<div key={key} className={"item"}>
					<strong>{name}</strong>
					<span>&nbsp;({subclasses})</span>
				</div>
			);
		}
	}
	subitems(){
		var cur_ctype = !this.props.hasOwnProperty('currentUrl') ||
						this.props.currentUrl.params.hasOwnProperty('id') ? "" : this.props.currentUrl.url.split('/')[1];
		if (this.state.loading) {
			return;
		}
		var items = [];
		for (var i = 0; i < Config.classifications.length; i++) {
		    let ctype = Config.classifications[i];
		    let name = ctype.charAt(0).toUpperCase() + ctype.slice(1);

		    if( ctype == 'membrane_systems' )
		    	name = 'Membrane Systems'

		    let route = '/' + ctype;
		    items.push(this.navitem(i, name, route, this.props.stats[ctype], cur_ctype === ctype));
		}
		return items;
	}

	render()
	{
		return(
			<div className="sidebar">
				
				<div className={this.state.loading ? "hidden" : "sidebar-nav"}>
						{this.subitems()}
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Config.loading_image}/>
				</div>
				<div className="server-link">
					<Link to="/permm_server_cgopm">
						<ImgWrapper
							className="server-banner"
							src={Config.permm_server_image} 
						/>
					</Link>
				</div>
			</div>
		);
	}
}


function mapStateToProps(state) {
  return {
		stats: state.stats,
		currentUrl: state.currentUrl,
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
		loadStats,
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
