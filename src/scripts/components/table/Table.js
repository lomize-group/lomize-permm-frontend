import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import axios from 'axios'
import appendQuery from 'append-query';

import Config from '../../Config.js';

import { Column, Table as VTable, WindowScroller, AutoSizer, CellMeasurer, CellMeasurerCache } from 'react-virtualized';
import './react-virtualized.scss';

import ImgWrapper from '../../helpers/img_wrapper/ImgWrapper.js';

import Search from '../search/Search.js';

import { Link } from 'react-router-dom';

import './Table.scss';

var CancelToken = axios.CancelToken;

class Table extends React.Component{
	constructor(props)
	{
		super(props);
		this.state = {
			data: {
				total_objects: 0,
				objects: [],
				sort: "",
				direction: ""
			},
			isPrinting: false,
			title: props.title,
			loading: true,
			search: props.search,
			display: false,
			header_height: 65,
			row_height: 42,
			style: {},
			name: '',
			src: ''
		};

		this.unmounted = false;
		this.requests = 0;

		this.gen_min_widths = () => {
			let width = 0;
			for (let i = 0; i < this.props.columns.length; i++) {
				if (this.props.columns[i].hasOwnProperty('width')){
					width += this.props.columns[i].width;
				}
				else {
					width += this.props.columns[i].minWidth;
				}
			}

			return width;
		}

		this.render_cell = (column) => {
			return (props) => {
				let row = props.rowData;
				row.onImgChange = (state) => { 
					this.setState(state); 
				};

				let color_class = (props.rowIndex % 2) === 0 ? "even" : "odd";

				return (
					<div className={color_class}>
						{ column.Cell(row) }
					</div>
				);
			}
		}

		this.render_header = (column) => {
			return () => {

				let sorted_class = "hidden";
				if (this.state.data.sort === column.id) {
					sorted_class = this.state.data.direction.toLowerCase();
				}

				return (
					<div className={column.id === '' ? "header-wrapper" : "header-wrapper sortable"}>
						{ column.Header() }
						<div className={sorted_class}></div>
					</div>
				)
			}
		}

		this.row_height = () => {
			return this.state.row_height;
		}


		this.render_body = () => {
			let items = [];
			for (var i = 0; i < this.props.columns.length; i++) {
				items.push(
					<Column
						key={i}
				    	headerRenderer={this.render_header(this.props.columns[i])}
				    	cellRenderer={this.render_cell(this.props.columns[i])}
						width={!this.props.columns[i].hasOwnProperty('width') ? this.props.columns[i].minWidth : this.props.columns[i].width}
						flexGrow={!this.props.columns[i].hasOwnProperty('flexGrow') ? 0 : this.props.columns[i].flexGrow}
						flexShrink={!this.props.columns[i].hasOwnProperty('flexGrow') ? 0 : 1}
						minWidth={!this.props.columns[i].hasOwnProperty('minWidth') ? 0 : this.props.columns[i].minWidth}
						maxWidth={!this.props.columns[i].hasOwnProperty('maxWidth') ? 0 : this.props.columns[i].maxWidth}
						dataKey={this.props.columns[i].id}
				    />
				);
			}
			return items;
		}

		this.fetch_data = (params, search = false, replace = false) => {
			if (!params.hasOwnProperty('pageSize')) {
				params.pageSize = 100;
			}
			let url = appendQuery(this.props.url, params);
			this.requests++;
			var req = this.requests;

			axios.get(url)
		    .then((res) => {
		    	if (this.unmounted || req !== this.requests) {
		    		return;
		    	}
		    	if (search && this.props.redirect && res.data.total_objects === 1) {
		    		if (replace) {
		    			this.props.currentUrl.history.replace('/molecules/' + res.data.objects[0].id);
		    			return;
		    		}

		    		this.props.currentUrl.history.push('/molecules/' + res.data.objects[0].id);
		    		return;
		    	}
		    	if (res.data.total_objects <= params.pageSize) {
		    		this.setState({
						data: res.data,
						loading: false
					});
		        	return;
		        }
		        this.setState({
		        	data: res.data
		        });

		        params.pageSize = res.data.total_objects;
		        this.fetch_data(params, search, replace);
		    });
		}

		this.handle_sort = (props) => {
			if (props.dataKey === '') {
				return;
			}
			let direction = 'ASC';
			if (props.dataKey === this.state.data.sort && this.state.data.direction === 'ASC'){
				direction = 'DESC';
			}
			let params = {
				search: this.state.search,
				sort: props.dataKey,
				direction: direction
			};  
		    this.fetch_data(params);
		    this.setState({
		    	loading: true,
		    	search: this.state.search,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: "",
					direction: ""
		    	}
		    });
		}

		this.handle_search = (value, replace = false) => {
			let params = {
				search: value,
				sort: this.state.data.sort,
				direction: this.state.data.direction
			}; 
		    this.fetch_data(params, true, replace);
		    this.setState({
		    	loading: true,
		    	search: value,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: this.state.data.sort,
					direction: this.state.data.direction
		    	},
		    	title: value === "" ? this.props.title : "Search Results for \"" + value + "\""
		    });
		}

		this.handle_no_rows = () => {
			return (
				<div className={this.state.loading ? "hidden" : "no-rows"}>
					no compounds found
				</div>
			);
		}
		if(props.hasOwnProperty('data')){
			this.state.data = props.data;
			if (props.data.total_objects > props.data.objects.length) {
				this.fetch_data({ 
					search: props.search,
					pageSize: props.data.total_objects
				}, props.search !== "");
			}
			else {
				this.state.loading = false;
			}
		}
		else {
			this.fetch_data({ search: props.search }, props.search !== "", true);
			if (props.search !== ""){
				this.state.title = "Search Results for \"" + props.search + "\""
			}
		}
		this.updateSize = this.updateSize.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.search !== this.props.search) {
			let params = {
				search: nextProps.search,
			}; 
		    this.fetch_data(params, true, true);
		    this.setState({
		    	loading: true,
		    	search: nextProps.search,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: '',
					direction: ''
		    	},
		    	species: [],
		    	title: nextProps.search === "" ? this.props.title : "Search Results for \"" + nextProps.search + "\""
		    });
		}
	}

	componentDidMount() {
		if (this.unmounted === false) {
			window.onbeforeprint = () => {
				this.setState({isPrinting: true})
			}
			window.onafterprint = () => {
				this.setState({isPrinting: false})
			}
		}
	}

	componentWillUnmount() {
		window.onbeforeprint = null;
		window.onafterprint = null;
		this.unmounted = true;
	}

	updateSize() {
		this.windowScrollerRef.updatePosition();
	}

	render()
	{
		var min_table_width = this.gen_min_widths();


		// this will render the full table if passed as a prop to a <VTable>.
		// this is important for printing!
		const overscanIndicesGetter = ({cellCount}) => ({
		  overscanStartIndex: 0,
		  overscanStopIndex: cellCount - 1,
		});

		return(
			<div className="light-table">
				<h2 className="table-header">
					{this.state.title + ' (' + this.state.data.total_objects + ' entries)'}
				</h2>
				<div className="search-section">
					<Search
						onSearch={this.handle_search}
						placeholder="Search table..."
						initial={this.state.search}
					/>
				</div>
				<div className="virtual-table">
					<WindowScroller ref={ref => { this.windowScrollerRef = ref }}>
	    				{({ height, isScrolling, onChildScroll, scrollTop }) => (
	    					<AutoSizer disableHeight>
	    						{({ width }) => (
									<VTable
										autoHeight
								        height={height}
								        isScrolling={isScrolling}
								        onScroll={onChildScroll}
								        scrollTop={scrollTop}
									    width={width > min_table_width ? width : min_table_width}
									    headerHeight={this.state.header_height}
									    rowHeight={this.row_height}
									    rowCount={this.state.data.objects.length}
									    rowGetter={({ index }) => this.state.data.objects[index]}
									    onHeaderClick={this.handle_sort}
									    noRowsRenderer={this.handle_no_rows}
									    overscanIndicesGetter={this.state.isPrinting ? overscanIndicesGetter : undefined}
									>
									    { this.render_body() }
									</VTable>
								)}
							</AutoSizer>
						)}
	  				</WindowScroller>
	  				<div className={this.state.display ? "floating-image" : "hidden"} style={this.state.style}>
						<div className="image-header">
							<span className="header-text">{this.state.name}</span>
						</div>
						<ImgWrapper src={this.state.src}/>
					</div>
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Config.loading_image}/>
				</div>
			</div>
		);
	}
}

Table.propTypes = {
	url: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	search: PropTypes.string,
	redirect: PropTypes.bool
};

Table.defaultProps = {
	search: "",
	redirect: true
};

function mapStateToProps(state) {
  return {
		 currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps, null, null, { withRef: true })(Table);