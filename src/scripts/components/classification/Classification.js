import React from 'react';

import Config from '../../Config.js';

import appendQuery from 'append-query';

import { connect } from 'react-redux';

import Info from '../info/Info.js';
import List from '../list/List.js';
import Table from '../table/Table.js';

import './Classification.scss';

import axios from 'axios';


class Classification extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state = {};

		this.unmounted = false;
		this.requests = 0;

		this.fetch = (cur_props) => {
			var return_state = {};
			let ctype = props.currentUrl.url.split('/')[1];

			// single entity
			if (cur_props.currentUrl.params.hasOwnProperty('id') || ctype === 'molecules'){
				let url = '';
				let redirect = true;
				let defaultSortTable = ctype;
				if (ctype === 'membrane_systems'){
					defaultSortTable = "experimental_measurements";
					url = Config.baseUrl + Config.classification[ctype].api.route + 
						'/' + props.currentUrl.params.id + 
						Config.classification["experimental_measurements"].api.route;
					redirect = false;
				}
				else if (ctype === "molecules") {
					url = Config.baseUrl + Config.classification[ctype].api.route;
				} 
				else {
					url = Config.baseUrl + Config.classification[ctype].api.route + 
						'/' + props.currentUrl.params.id + 
						Config.classification["molecules"].api.route;
					redirect = false;
				}
				
				return_state.header = 'Loading...';
				return_state.loading = true;

				this.requests++;
				var req = this.requests;
				
				// fetch data here
				let params = {
					search: cur_props.currentUrl.query.search,
					sort: Config.columns[defaultSortTable][0].id, // default sort by first field in table
					pageSize: 100
				};
				url = appendQuery(url, params);
				axios.get(url)
				.then((res) => {
					if (this.unmounted || req !== this.requests) {
			    		return;
			    	}
			    	if (redirect && res.data.total_objects === 1) {
		    			this.props.currentUrl.history.replace('/molecules/' + res.data.objects[0].id);
		    			return;
			    	}
			    	var newstate = {
						header: res.data.name,
						loading: false
					};
					if (ctype === 'membrane_systems'){
						newstate.tableData = {
							title: "Experimental Measurements",
							url: url,
							columns: Config.columns.experimental_measurements,
							search: cur_props.currentUrl.query.search,
							data: res.data,
							redirect: false,
				    	};
				    	newstate.description = Config.classification[ctype][newstate.header].description;
					}
					else if (ctype === "molecules") {
						newstate.tableData = {
							title: "All molecules in PerMM",
							url: url,
							columns: Config.columns[ctype],
							search: cur_props.currentUrl.query.search,
							data: res.data
						};
					} 
					else {
						newstate.tableData = {
							title: "Molecules",
							url: url,
							columns: Config.columns[ctype],
							search: cur_props.currentUrl.query.search,
							data: res.data,
							redirect: false
				    	};
					}

					let new_cols = [];	
					console.log(res)
					if (ctype === 'membrane_systems')
						new_cols = Config.add_membrane_system_columns(res.data);	
					else
						new_cols = Config.add_molecule_columns(res.data);

					newstate.tableData.columns = [].concat(newstate.tableData.columns, new_cols);
					this.setState(newstate);
			    });
			}
			// only a list
			else {
				return_state.listHeader = ctype.charAt(0).toUpperCase() + ctype.slice(1);
				return_state.loading = false;
				if (ctype === "membrane_systems"){
					return_state.listHeader = "Membrane Systems";
				}

				return_state.listData = {
					url: Config.baseUrl + Config.classification[ctype].api.route,
					ctype: ctype,
					sub_type: Config.classification[ctype].child
				};
			}

			return return_state;
		}

		this.state = this.fetch(props);

		this.renderClassification = () => {
			var items = [];
			if (this.state.hasOwnProperty('header')){
				items.push(
					<div key={0} className="page-header">
						{ this.state.header }
					</div>
				);
			}
			else if (this.state.hasOwnProperty('listHeader')) {
				items.push(
					<div key={0} className="page-header">
						{ this.state.listHeader }
					</div>
				);
			}

			if (this.state.hasOwnProperty('description')){
				items.push(
					<div key={1}>
						{ this.state.description }
					</div>
				)
			}

			if (this.state.loading){
				items.push(
					<div className="info-loading" key={2}>
						<img className="loading-image" src={Config.loading_image}/>
					</div>
				);
			}

			if (this.state.hasOwnProperty('listData') && this.state.hasOwnProperty('header')){
				items.push(
					<div key={3} className="list-section">
						<div className="list-header">
							{ this.state.listHeader }:
						</div>
						<List {...this.state.listData}/>
					</div>
				);
			}
			else if (this.state.hasOwnProperty('listData') && !this.state.hasOwnProperty('header')){
				items.push(
					<div key={3} className="list-section">
						<List {...this.state.listData}/>
					</div>
				);
			}

			if (this.state.hasOwnProperty('tableData')){
				items.push(
					<div key={4} className="table-section">
						<Table {...this.state.tableData}/>
					</div>
				);
			}

			return items;
		}
	}
	
	componentWillReceiveProps(nextProps) {
		this.setState(this.fetch(nextProps));
	}

	componentWillUnmount() {
		this.unmounted = true;
	}

	render()
	{
		return(
			<div className="classification">
				{ this.renderClassification() }
			</div>
		);
	}
}


function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(Classification);
