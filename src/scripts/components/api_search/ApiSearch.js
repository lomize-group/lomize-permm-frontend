import React from 'react';
import PropTypes from 'prop-types';
import appendQuery from 'append-query';
import axios from 'axios';

import Config from '../../Config.js';

import 'bootstrap';
import './ApiSearch.scss';

class ApiSearch extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
		this.search = "";

		this.handle_search = () => {
			if (this.props.onSearch) {
				this.props.onSearch();
			}

			let params = {
				search: this.search
			};
			if (this.props.searchBy) {
				params.search_by = this.props.searchBy;
			}

			const url = appendQuery(Config.baseUrl + "/" + Config.classification.molecules.api.route, params);
			axios.get(url)
			.then((res) => {
				this.props.onSearchComplete(res.data);
			});
		}

		this.handle_text_change = (event) => {
			this.search = event.target.value;
		} 

		this.handle_key_press = (event) => {
			if (this.props.useEnterSubmit && event.key === 'Enter'){
				this.handle_search();
			}
		}
	}


	render()
	{
		return(
			<div className="api-search">
				<div className="input-group">
					<input
						type="text"
						id="search-box"
						className={"search-bar form-control input-sm " + this.props.className}
						placeholder="Search molecules"
						onKeyPress={this.handle_key_press} 
						onChange={this.handle_text_change}
					/>
					<span className="submit-button input-group-addon" onClick={this.handle_search}>
					    <i>search</i>
					</span>
				</div>
			</div>
		);
	}
}

ApiSearch.propTypes = {
	className: PropTypes.string,
	useEnterSubmit: PropTypes.bool,
	onSearchComplete: PropTypes.func.isRequired,
	onSearch: PropTypes.func,
	searchBy: PropTypes.string
}

ApiSearch.defaultProps = {
	className: "",
	useEnterSubmit: false
}

export default ApiSearch;
