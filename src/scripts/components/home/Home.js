import React from 'react';
import ReactDOM from 'react-dom';

import Config from '../../Config.js';

// import PDB from '../protein/pdb/PDB.js';

import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ImgWrapper from '../../helpers/img_wrapper/ImgWrapper.js';

import axios from 'axios';

import './Home.scss';

class Home extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			header: "Permeability of Molecules across Membranes (PerMM) server and database",
			id: 0,
			name: "",
			pdbid: "",
		};

		this.renderImage = () => {
			if (this.state.pdbid === "") {
				return "";
			}
			return (
				<div>
					<Link to={"/proteins/" + this.state.id}>
						<ImgWrapper className="img-style" src={Config.pdb_image(this.state.pdbid)}/>
					</Link>
					<div className="protein-link">
						<Link to={"/proteins/" + this.state.id}>{this.state.name} pdb-{this.state.pdbid}</Link>
					</div>
					<hr/>
					<div className="protein-link">
						<a href={Config.pdb_file(this.state.pdbid)} download>Download File: {this.state.pdbid}.pdb</a>
					</div>
				</div>
			);
		}
	}

	makeClassStat(key, classtype) {
		return(
			<div key={key} className="classtype-stat">
				<p><span className="green italic">Class:</span> <Link to={"/classes/" + classtype.id}>{classtype.name}</Link> - <span className="bold">({classtype.structure_groupings}/{classtype.complex_structures})</span></p>
			</div>
			)
	}

	makeTypeStat(key, type) {
		var classStats = []
		for (var i = 0; i < type.classes.length; i++) {
			var classtype = type.classes[i].table
			classStats.push(this.makeClassStat(i, classtype));
		}
		return(
			<div key={key} className="type-stat">
				<p><span className="green italic">Type:</span> <Link to={"/types/" + type.id}>{type.name}</Link> - <span><strong>({type.structure_groupings}/{type.complex_structures})</strong></span></p>
				<div className="classtype-stat-holder">
					{classStats}
				</div>
			</div>
			)
	}

	render() {
		return(
			<div className="home">
				<div className="page-header">
	                {this.state.header}
	            </div>
	            <div className="home-text">
	            	<div className="indented">
	            		<p>The PerMM web server is a computational tool for theoretical assessment of passive permeability of molecules across the lipid bilayer. The underlying thermodynamics-based method is based on inhomogeneous solubility-diffusion theory and operates with atomic 3D structures of solutes and the anisotropic solvent model of the DOPC bilayer characterized by transbilayer profiles of dielectric and hydrogen-bonding capacity parameters. The PerMM method calculates membrane binding affinity, energy profiles along the bilayer normal, and permeability coefficients of diverse molecules across different membranes. Calculated permeability coefficients reproduce experimental data for diverse membrane systems, such as BLM (R2=0.88, RMSE=1.15 log units), PAMPA (R2=0.75, RMSE=1.59 log units), BBB (R2=0.69, RMSE=0.87 log units), and Caco-2/MDCK cells (R2=0.52, RMSE=0.89 log units).</p>
	            		<p>The PerMM database collects computationally-generated data along with experimental permeability coefficients obtained for artificial and natural membranes, including BLM, PAMPA, Caco-2/MDCK cells, and BBB. It currently contains ~500 molecules, ranging from small organic compounds to natural products of diverse chemical structures. The database provides downloadable coordinate files and interactive visual information to represent optimal localization and translocation pathways of molecules across the lipid bilayer. Presented data allow assessment of the contribution of the passive diffusion to transport of drugs and other compounds across biological membranes.</p>
	            	</div>
					<div className="indented">
						<p><b>In citing the PerMM method and database please refer to</b></p>
                    </div>
					<p>
						<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Lomize%20AL%5BAuthor%5D&cmd=DetailsSearch" className="external" target="_blank">
							Lomize AL
						</a>{", "}
						<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Pogozheva+ID%5BAuthor%5D" className="external" target="_blank">
							Pogozheva ID
						</a>.
						<b> Physics-based method for modeling passive membrane permeability and translocation pathways of bioactive molecules.</b>
						<i> J Chem Inf Model.</i>
						<b> 51</b>:3198-3213.
						{" "}
						<a href="https://www.ncbi.nlm.nih.gov/pubmed/31259555" className="external" target="_blank">PubMed</a>
					</p>
					<p>
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Lomize%20AL%5BAuthor%5D&cmd=DetailsSearch" className="external" target="_blank">
						Lomize AL
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Hage+JM%5BAuthor%5D" className="external" target="_blank">
						Hage JM
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Schnitzer+K%5BAuthor%5D" className="external" target="_blank">
						Schnitzer K
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Golobokov+K%5BAuthor%5D" className="external" target="_blank">
						Golobokov K
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=LaFaive+MB%5BAuthor%5D" className="external" target="_blank">
						LaFaive MB
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Forsyth+AC%5BAuthor%5D" className="external" target="_blank">
						Forsyth AC
					</a>{", "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Pogozheva+ID%5BAuthor%5D" className="external" target="_blank">
						Pogozheva ID
					</a>.
					<b> PerMM: A web tool and database for analysis of passive membrane permeability and translocation pathways of bioactive molecules.</b>
					<i> J Chem Inf Model.</i>
					<b> 59</b>:3094-3099.
					{" "}
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/31259547" className="external" target="_blank">PubMed</a>
					</p>
                    <br/>
	            </div>
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
		stats: state.stats,
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(Home);
