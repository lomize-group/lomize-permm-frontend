import React from "react";

import ExternalLink from "../../helpers/external_link/ExternalLink.js";

import "./Footer.scss";

class Footer extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}


	render()
	{
		return(
			<div className="footer-box">
				<div className="maintext">
				&copy; 2019 The Regents of the University of Michigan | <ExternalLink href="https://creativecommons.org/licenses/by/3.0/">License</ExternalLink>
				</div>
			</div>
		);
	}
}

export default Footer;
