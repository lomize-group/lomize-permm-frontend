import React from "react";
import { Link } from "react-router-dom";

// if I wanted to configure the anchor tags
// import { configureAnchors } from "react-scrollable-anchor"
// configureAnchors({offset: -60, scrollDuration: 200})

export type ComputationServer = "memprot" | "cgopm" | "local";

const Config = {
	columns: {
		groups: [
			{
				Header: () => <div className="header-cell" title="Molecule Name"><div>Molecule Name</div></div>,
				id: "name",
				flexGrow: 3,
				minWidth: 100,
				Cell: row => <div className="cell" title={row.name}>
					<div><Link to={"/molecules/" + row.id}>{row.name}</Link></div>
				</div>

			},
			{
				Header: () => <div className="header-cell" title="Class"><div>Class</div></div>,
				id: "class_type_name_cache",
				flexGrow: 3,
				minWidth: 100,
				Cell: row => <div className="cell" title={row.class_type_name_cache}>
					<div><Link to={"/classes/" + row.class_type_id}>{row.class_type_name_cache}</Link></div>
				</div>
			},
			{
				Header: () => <div className="header-cell" title="logPermCalc"><div>LogP<sub>0</sub> Calc</div></div>,
				id: "logp0_blm",
				width: 50,
				Cell: row => <div className="cell" title={row.logp0_blm.toFixed(2)}><div>{row.logp0_blm.toFixed(2)}</div></div>
			},
		],

		classes: [
			{
				Header: () => <div className="header-cell" title="Molecule Name"><div>Molecule Name</div></div>,
				id: "name",
				flexGrow: 3,
				minWidth: 100,
				Cell: row => <div className="cell" title={row.name}>
					<div><Link to={"/molecules/" + row.id}>{row.name}</Link></div>
				</div>

			},
			{
				Header: () => <div className="header-cell" title="Group"><div>Group</div></div>,
				id: "group_name_cache",
				flexGrow: 3,
				minWidth: 60,
				Cell: row => <div className="cell" title={row.group_name_cache}>
					<div><Link to={"/groups/" + row.group_id}>{row.group_name_cache}</Link></div>
				</div>
			},
			{
				Header: () => <div className="header-cell" title="logPermCalc"><div>LogP<sub>0</sub> Calc</div></div>,
				id: "logp0_blm",
				width: 50,
				Cell: row => <div className="cell" title={row.logp0_blm.toFixed(2)}><div>{row.logp0_blm.toFixed(2)}</div></div>
			},
		],

		experimental_measurements: [
			{
				Header: () => <div className="header-cell" title="Name"><div>Name</div></div>,
				id: "molecule_name_cache",
				flexGrow: 3,
				minWidth: 100,
				Cell: row => <div className="cell" title={row.molecule_name_cache}>
					<div><Link to={"/molecules/" + row.molecule_id}>{row.molecule_name_cache}</Link></div>
				</div>

			},
			{
				Header: () => <div className="header-cell" title="Class"><div>Class</div></div>,
				id: "class_type_name_cache",
				flexGrow: 3,
				minWidth: 100,
				Cell: row => <div className="cell" title={row.class_type_name_cache}>
					<div><Link to={"/classes/" + row.class_type_id}>{row.class_type_name_cache}</Link></div>
				</div>
			},
			{
				Header: () => <div className="header-cell" title="Group"><div>Group</div></div>,
				id: "group_name_cache",
				flexGrow: 3,
				minWidth: 60,
				Cell: row => <div className="cell" title={row.group_name_cache}>
					<div><Link to={"/groups/" + row.group_id}>{row.group_name_cache}</Link></div>
				</div>
			}
		],
		molecules: [
			{
				Header: () => <div className="header-cell" title="Molecule Name"><div>Molecule Name</div></div>,
				id: "name",
				flexGrow: 3,
				minWidth: 120,
				// maxWidth: 120,
				Cell: row => <div className="cell" title={row.name}>
					<div><Link to={"/molecules/" + row.id}>{limit_text_length(row.name)}</Link></div>
				</div>

			},
			{
				Header: () => <div className="header-cell" title="Class"><div>Class</div></div>,
				id: "class_type_name_cache",
				flexGrow: 3,
				minWidth: 104,
				// maxWidth: 108,
				Cell: row => <div className="cell" title={row.class_type_name_cache}>
					<div><Link to={"/classes/" + row.class_type_id}>{limit_text_length(row.class_type_name_cache)}</Link></div>
				</div>
			},
			{
				Header: () => <div className="header-cell" title="Group"><div>Group</div></div>,
				id: "group_name_cache",
				flexGrow: 3,
				minWidth: 58,
				Cell: row => <div className="cell" title={row.group_name_cache}>
					<div><Link to={"/groups/" + row.group_id}>{row.group_name_cache}</Link></div>
				</div>
			},
			{
				Header: () => <div className="header-cell" title="logPermCalc"><div>LogP<sub>0</sub> BLM Calc</div></div>,
				id: "logp0_blm",
				width: 50,
				Cell: row => {
					return <div className="cell" title={row.logp0_blm.toFixed(2)}><div>{row.logp0_blm.toFixed(2)}</div></div>
				}
			},
		],
	},
	downloads: [
		"groups",
		"classes",
		"membrane_systems",
		"molecules",
		"experimental_measurements",
	],
	classifications: [
		"groups",
		"classes",
		"membrane_systems",
		"molecules",
	],
	classification: {
		groups: {
			api: {
				route: "/groups",
				accessor: {
					object: "group",
					count: "groups_count",
				},
			},
			child: "molecules",
		},
		classes: {
			api: {
				route: "/classtypes",
				accessor: {
					object: "class_type",
					count: "class_types_count",
				},
			},
			child: "molecules",
		},
		experimental_measurements: {
			api: {
				route: "/experimental_measurements",
				accessor: {
					object: "experimental_measurement",
					count: "experimental_measurements_count",
				},
			},
			parents: ["membrane_systems"],
		},
		membrane_systems: { // aka membranes
			api: {
				route: "/membrane_systems",
				accessor: {
					object: "membrane_system",
					count: "membrane_systems_count",
				},
			},
			parents: [],
			child: "experimental_measurements",
			BLM: {
				experimental_header: <div>LogP<sub>0</sub> BLM Exp</div>,
				description: <div>
					<p>Black lipid membranes (BLMs) are formed by applying a solution of specified lipids (often, lecithin) dispersed in a nonpolar solvent (e.g. n-decane) on a small hole in Teflon sheet separating two aqueous solutions. Permeation of organic molecules through BLM is studied by either the radiotracer method or HPLC
									[<a className="external" target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/1420875">1</a>], or by measuring aqueous boundary layer pH gradients (for ionizable molecules)
									[<a className="external" target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/8290481">2</a>, <a className="external" target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/8369403">3</a>].</p>
				</div>
			},
			"PAMPA-DS(Po)": {
				experimental_header: <div>LogP<sub>0</sub> PAMPA<sub>DS</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) is an experimental assay quantifying the passive diffusive permeability of artificial membrane systems. The PAMPA-DS assay uses the lecithin-based double sink model, where pH gradients between the donor chamber (pH from 5.0 to 7.4) and the receiver chamber (pH=7.4) are introduced. This assay has “gastrointestinal tract” lipid formulation (20% lipid mixture of PC, PE, PI, PA, triglycerids). The multilamellar bilayers are expected to form inside the filter channels contacting with aqueous buffer solution. Lipid-coated PVDF filters separate donor chamber containing 0.5% DMSO from acceptor chamber containing surfactant in micellar form. A lipophilic scavenger in acceptor chamber (surfactant of BSA) reduces the membrane retention of hydrophobic compounds and makes their permeation unidirectional.</p>
					<p>The resulting <i>intrinsic</i> permeability coefficients (P<sub>0</sub>) represent experimentally obtained values corrected for effect of pH and permeability through the aqueous boundary layers adjacent at both sides of the membrane
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]. <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules.</p>
				</div>
			},
			"PAMPA-DS(Pm-6.5)": {
				experimental_header: <div>LogP<sub>m-6.5</sub> PAMPA<sub>DS</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) is an experimental assay quantifying the passive diffusive permeability of artificial membrane systems. The PAMPA-DS assay uses the lecithin-based double sink model, where pH gradients between the donor chamber (pH from 5.0 to 7.4) and the receiver chamber (pH=7.4) are introduced. This assay has “gastrointestinal tract” lipid formulation (20% lipid mixture of PC, PE, PI, PA, triglycerids).</p>
					<p><i>Pm-6.5</i> represents the membrane permeability at pH 6.5 calculated from the intrinsic <i>P<sub>0</sub></i> value and known pK<sub>a</sub> values of a molecule (25 ° C, 0.01 M ionic strength). <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules, i.e. the maximum possible value that the membrane permeability can reach.
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]</p>
				</div>
			},
			"PAMPA-DS(Pm-7.4)": {
				experimental_header: <div>LogP<sub>m-7.4</sub> PAMPA<sub>DS</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) is an experimental assay quantifying the passive diffusive permeability of artificial membrane systems. The PAMPA-DS assay uses the lecithin-based double sink model, where pH gradients between the donor chamber (pH from 5.0 to 7.4) and the receiver chamber (pH=7.4) are introduced. This assay has “gastrointestinal tract” lipid formulation (20% lipid mixture of PC, PE, PI, PA, triglycerids).</p>
					<p><i>Pm-7.4</i> represents the membrane permeability at pH 7.4 calculated from the intrinsic <i>P<sub>0</sub></i> value and known pK<sub>a</sub> values of a molecule (25 ° C, 0.01 M ionic strength). <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules, i.e. the maximum possible value that the membrane permeability can reach.
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]</p>
				</div>
			},
			"PAMPA-DOPC(Po)": {
				experimental_header: <div>LogP<sub>0</sub> PAMPA<sub>DOPC</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) contains PVDF filter disc coated by 2% w/v dodecane solution of synthetic DOPC. The donor chamber holds 0.5% DMSO. The resulting <i>intrinsic</i> permeability coefficients (<i>P<sub>0</sub></i>) represent experimentally obtained values corrected for effect of pH and permeability through the aqueous boundary layer adjacent at both sides of the membrane
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]. <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules, i.e. the maximum possible value that the membrane permeability can reach.</p>
				</div>
			},
			"PAMPA-BBB(Po)": {
				experimental_header: <div>LogP<sub>0</sub> PAMPA<sub>BBB</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) aimed to reproduce <i>in situ</i> rodent brain perfusion PS measurements. PAMPA-BBB contains PVDF filter disc coated by 10% porcine brain lipid extract (PC, PE, PS, PI, PA, cerebrosides) in viscous alkane solvent. The donor chamber holds 0.5% DMSO, the acceptor chamber contains surfactant in micellar form. The resulting <i>intrinsic</i> permeability coefficients (<i>P<sub>0</sub></i>) represent experimentally obtained values corrected for effect of pH and permeability through the aqueous boundary layer adjacent at both sides of the membrane
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]. <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules, i.e. the maximum possible value that the membrane permeability can reach.</p>
				</div>
			},
			"PAMPA-BBB(Pm)": {
				experimental_header: <div>LogP<sub>m</sub> PAMPA<sub>BBB</sub> Exp</div>,
				description: <div>
					<p>The Parallel Artificial Membrane Permeability Assay (PAMPA) aimed to reproduce <i>in situ</i> rodent brain perfusion PS measurements. PAMPA-BBB contains PVDF filter disc coated by 10% porcine brain lipid extract (PC, PE, PS, PI, PA, cerebrosides) in viscous alkane solvent.  The donor chamber holds 0.5% DMSO, the acceptor chamber contains surfactant in micellar form. The resulting <i>intrinsic</i> permeability coefficients (<i>P<sub>0</sub></i>) represent experimentally obtained values corrected for effect of pH and permeability through the aqueous boundary layer adjacent at both sides of the membrane
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch7">1</a>]. <i>P<sub>m</sub></i> represents the membrane permeability at pH 7.4 calculated from the intrinsic <i>P<sub>0</sub></i> value and known pK<sub>a</sub> values of a molecule (25 ° C, 0.01 M ionic strength). <i>P<sub>0</sub></i> refers to the membrane permeability of the neutral form of the ionized molecules, i.e. the maximum possible value that the membrane permeability can reach.</p>
				</div>
			},
			"Caco-2(Po)": {
				experimental_header: <div>LogP<sub>0</sub> Caco2 Exp</div>,
				description: <div>
					<p>
						<i>Intrinsic</i> permeability data for intestinal cellular membranes, <i>P<sub>0</sub></i>, is the maximum possible value that the membrane permeability can reach. <i>P<sub>0</sub></i> values were calculated from high-quality apparent permeability (<i>P<sub>app</sub></i>) values measured in Caco-2 (colon adenocarcinoma cell line) and MDCK (Madin-Darby canine kidney) epithelial cell lines. The membrane permeability data were collected from 55 studies
										[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch8">1</a>] and corrected for all nontrancellular effects using the pCEL-X computer program
										(<a className="external" target="_blank" href="http://www.in-adme.com/pcel_x.html">http://www.in-adme.com/pcel_x.html</a>). To cancel contributions from the active or the facilitated transport, the average was taken between apical-to-basolateral and basolateral-to-apical measurements for compounds that are known as substrates for efflux/uptake carrier-mediated transport.
									</p>
				</div>
			},
			"Caco-2(Pc)": {
				experimental_header: <div>LogP<sub>c</sub> Caco2 Exp</div>,
				description: <div>
					<p><i>P<sub>c</sub></i> represents the transcellular permeability of apical and basolateral bilayer membranes of the cells at pH 6.5. Its maximal possible value is the permeability of the neutral species, <i>P<sub>0</sub></i>; its minimum possible value is the permeability of the ionized species, <i>P<sub>i</sub></i>. Unlike PAMPA, the cell-based <i>P<sub>0</sub></i> and <i>P<sub>i</sub></i> values may be carrier-mediated. To cancel contributions from the active or the facilitated transport, the average was taken between apical-to-basolateral and basolateral-to-apical measurements for compounds that are known as substrates for efflux/uptake carrier-mediated transport.
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch8">1</a>]</p>
				</div>
			},
			"BBB(Po)": {
				experimental_header: <div>LogP<sub>0</sub> BBB Exp</div>,
				description: <div>
					<p><i>Intrinsic</i> permeability data obtained <i>in vivo</i> for Blood-Brain Barrier (BBB) were compiled by Avdeef [6]. Most data were obtained from <i>in situ</i> rodent brain perfusion method, referred to permeation from saline at pH 7.4 and corrected for ionization, while some were based on the <i>in vivo</i> intravenous injections (i.v.). The i.v. data were not used for lipophilic compounds that are known to bind plasma proteins. Data were selected for efflux-minimized conditions, when PS values were measured during inhibition of carrier- or transporter-mediated permeability or in knock-out mouse models.
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch9">1</a>]</p>
				</div>
			},
			"BBB(PS)": {
				experimental_header: <div>LogPS BBB Exp</div>,
				description: <div>
					<p><i>In situ</i> brain perfusion technique is used for the <i>in vivo</i> measurement of the initial rate of brain penetration at the lumenal BBB membrane. The permeability-surface area product, PS, is the product of the luminal permeability, <i>P<sub>c</sub></i> (cm s<sup>-1</sup> ), and the endothelial surface area, S (cm<sup>2</sup>g<sup>-1</sup>). PS is the transfer constant for the initial brain transport of a drug corrected for the velocity of the perfusion flow. Data were selected for efflux-minimized conditions, when PS values were measured during inhibition of carrier- or transporter-mediated permeability or in knock-out mouse models.
									[<a className="external" target="_blank" href="https://onlinelibrary.wiley.com/doi/10.1002/9781118286067.ch9">1</a>]</p>
				</div>
			}
		},
		molecules: {
			api: {
				route: "/molecules",
				accessor: {
					object: "molecules",
					count: "molecules_count",
				},
			},
			parents: [
				"groups",
				"classes",
				"membrane_systems",
			],
			pubchem: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "https://pubchem.ncbi.nlm.nih.gov/compound/" + pdb_id;
			},
			drugbank: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "https://www.drugbank.ca/drugs/" + pdb_id;
			},
			chebi: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:" + pdb_id;
			},
			wikipedia: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "https://en.wikipedia.org/wiki/" + pdb_id;
			},
			metabolome: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "http://www.hmdb.ca/metabolites/" + pdb_id;
			},
			pdb: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "https://www3.rcsb.org/ligand/" + pdb_id;
			},
			kegg: (pdb_id) => {
				if (pdb_id === undefined || pdb_id === "")
					return null;

				return "http://www.kegg.jp/entry/" + pdb_id;
			},

		},
	},

	//baseUrl: "https://lomize-group-permm.herokuapp.com",
	baseUrl: "https://opm-back.cc.lehigh.edu/permm-backend",
	// baseUrl: "http://localhost:3000",
	pubchem_image: (pdb_id) => {
		return "https://storage.googleapis.com/permm-assets/images/pubchem/" + pdb_id + ".png"
	},
	ppm_image: (pdb_id) => {
		return "https://storage.googleapis.com/permm-assets/images/ppm/" + pdb_id + "1.png"
	},
	pdb_pathway_file: (pdb_id) => {
		return "https://storage.googleapis.com/permm-assets/pdb/permm/" + pdb_id + "out.pdb"
	},
	pdb_bound_state_file: (pdb_id) => {
		return "https://storage.googleapis.com/permm-assets/pdb/ppm/" + pdb_id + "1.pdb"
	},
	pdb_source_file: (pdb_id) => {
		return "https://storage.googleapis.com/permm-assets/pdb/source/" + pdb_id + ".pdb"
	},
	pubmed: (pubmed) => {
		return "https://www.ncbi.nlm.nih.gov/pubmed/" + pubmed
	},
	load_script: (script_src, onLoadCallback) => {
		const script = document.createElement("script");
		script.src = script_src;
		if (onLoadCallback)
			script.addEventListener("load", onLoadCallback, false);
		document.head.appendChild(script);
	},
    computation_servers: {
        memprot: "https://memprot.org/",
        cgopm: "https://cgopm.cc.lehigh.edu/",
        local: "http://localhost:9000/",
    },
	no_image: "/images/pdb_fallback.png",
	loading_image: "/images/loading.gif",
	loading_image_small: "/images/loading_small.gif",
	permm_server_image: "https://storage.googleapis.com/permm-assets/images/ui_assets/Logo_PerMM3.png",
	pics: {
		adrei: "https://storage.googleapis.com/permm-assets/images/portraits/Andrei2.jpg",
		irina: "https://storage.googleapis.com/permm-assets/images/portraits/Ira2.jpg",
		alexey: "https://storage.googleapis.com/permm-assets/images/portraits/Alexey.jpg"
	},
	bound_states_tar: "https://storage.googleapis.com/permm-assets/pdb/ppm/bound_states.tar",
	pathway_tar: "https://storage.googleapis.com/permm-assets/pdb/permm/pathways.tar",
	source_tar: "https://storage.googleapis.com/permm-assets/pdb/source/source_files.tar",
	molecule_list_tar: "https://storage.googleapis.com/permm-assets/database/Molecules.sql.tar",
	full_database_tar: "https://storage.googleapis.com/permm-assets/database/permm.sql.tar"
}

Config.add_molecule_columns = (data) => {
	let new_cols = [];
	for (var i = 0; i < data.membrane_headers.length; i++) {
		const { id, name } = data.membrane_headers[i];
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="logPermMembrane">
					{Config.classification.membrane_systems[name].experimental_header}
				</div>,
				id: "membrane_system_id=" + id,
				width: name.length <= 10 ? 42 : 68,
				Cell: row => {
					const { log_perm_coeff, charged_state } = row.membrane_systems[id] || {};
					return (
						<div className="cell">
							<div>
								{
									log_perm_coeff && (charged_state !== "ionized" || name !== "BLM") &&
									Number.parseFloat(log_perm_coeff).toFixed(2)
								}
							</div>
						</div>
					);
				}
			}
		);
	}
	return new_cols
}

Config.add_membrane_system_columns = (data) => {
	let new_cols = [];

	const getFloatIfExists = val => val ? val.toFixed(2) : null;

	// add calculated columns
	if (data.name === "BLM") {
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="logPermCalcIonized">
					<div>LogP<sub>0</sub> Calc Ionized</div>
				</div>,
				id: "molecule_logp_cache",
				width: 50,
				Cell: row => <div className="cell" title={row.molecule_logp_cache.toFixed(2)}>
					<div>{row.molecule_logp_cache.toFixed(2)}</div>
				</div>
			},
		);
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="logPermCalcNeutral">
					<div>LogP<sub>0</sub> Calc Neutral</div>
				</div>,
				id: "molecule_logp0_cache",
				width: 50,
				Cell: row => <div className="cell" title={row.molecule_logp0_cache.toFixed(2)}>
					<div>{row.molecule_logp0_cache.toFixed(2)}</div>
				</div>
			}
		);
	}
	else if (data.name === "PAMPA-DS(Po)" || data.name === "Caco-2(Po)" || data.name === "BBB(Po)") {
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="logPermCalc">
					<div>LogP<sub>0</sub> Calc</div>
				</div>,
				id: "molecule_logp0_cache",
				width: 50,
				Cell: row => <div className="cell" title={row.molecule_logp0_cache ? row.molecule_logp0_cache.toFixed(2) : null}>
					<div>{getFloatIfExists(row.molecule_logp0_cache)}</div>
				</div>
			}
		);
	}
	else if (data.name === "PAMPA-DS(Pm-6.5)") {
		const subscript_num =
			new_cols.push(
				{
					Header: () => <div className="header-cell" title="logPermCalc">
						<div>LogP<sub>m-6.5</sub> Calc</div>
					</div>,
					id: "molecule_logp0_cache",
					width: 70,
					Cell: row => <div className="cell" title={row.molecule_logp0_cache ? row.molecule_logp0_cache.toFixed(2) : null}>
						<div>{getFloatIfExists(row.molecule_logp0_cache)}</div>
					</div>
				}
			);
	}
	else if (data.name === "PAMPA-DS(Pm-7.4)") {
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="logPermCalc">
					<div>LogP<sub>m-7.4</sub> Calc</div>
				</div>,
				id: "molecule_logp0_cache",
				width: 70,
				Cell: row => <div className="cell" title={row.molecule_logp0_cache ? row.molecule_logp0_cache.toFixed(2) : null}>
					<div>{getFloatIfExists(row.molecule_logp0_cache)}</div>
				</div>
			}
		);
	}


	// add experimental column
	new_cols.push(
		{
			Header: () => <div className="header-cell" title="logPermMembrane">
				{Config.classification.membrane_systems[data.name].experimental_header}
			</div>,
			id: "log_perm_coeff",
			width: 75,
			Cell: row => <div className="cell" title={row.log_perm_coeff.toFixed(2)}>
				<div>{row.log_perm_coeff.toFixed(2)}</div>
			</div>
		}
	);

	// other specific columns to add
	if (data.name === "BLM") {
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="chargedState"><div>Data Type</div></div>,
				id: "charged_state",
				width: 120,
				Cell: row => <div className="cell" title={row.charged_state}>
					<div>{row.charged_state}</div>
				</div>
			}
		);
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="Reference"><div>Reference</div></div>,
				id: "reference",
				flexGrow: 3,
				minWidth: 120,
				Cell: row => <div className="cell" title={row.reference}>
					<div><a target="_blank" href={"https://www.ncbi.nlm.nih.gov/pubmed/" + row.pubmed}>{row.reference}</a></div>
				</div>
			}
		);
	}
	else {
		new_cols.push(
			{
				Header: () => <div className="header-cell" title="Reference"><div>Reference</div></div>,
				id: "reference",
				flexGrow: 3,
				minWidth: 120,
				Cell: row => <div className="cell" title={row.reference}>
					<div>{row.reference}</div>
				</div>
			}
		);
	}

	return new_cols
}

function limit_text_length(text) {
	return text.length > 30 ?
		text.slice(0, 30).trim() + "..." : text
}



export default Config;
