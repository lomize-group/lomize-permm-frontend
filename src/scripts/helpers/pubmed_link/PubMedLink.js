import React from "react";
import PropTypes from "prop-types";

import ExternalLink from "../external_link/ExternalLink.js";


const PUBMED_BASE_URL = "https://www.ncbi.nlm.nih.gov/pubmed/";

class PubMedLink extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}

	render()
	{
		return <ExternalLink href={PUBMED_BASE_URL + this.props.id}>{this.props.children}</ExternalLink>
	}
}

PubMedLink.propTypes = {
	id: PropTypes.string.isRequired
};
PubMedLink.defaultProps = {
	children: "PubMed"
};

export default PubMedLink;
