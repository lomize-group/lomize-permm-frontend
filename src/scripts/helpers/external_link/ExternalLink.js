import React from "react";
import PropTypes from "prop-types";

import "./ExternalLink.scss";

class ExternalLink extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}

	render()
	{
		return <a className="external-link" target="_blank" href={this.props.href}>{this.props.children}</a>;
	}
}

ExternalLink.propTypes = {
	href: PropTypes.string
};
ExternalLink.defaultProps = {
	href: ""
};

export default ExternalLink;
